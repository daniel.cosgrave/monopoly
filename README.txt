**** Team ***
Daniel Cosgrave	
Emma Lambkin 
Team 3

*** Source Code ***
Source Code and data files in /src
All Source Code is organised into appropriate folders 
e.g
code for cards is in /src/cards

** Diagrams **
Diagrams are in a folder called Diagrams
We were unable to get Papyrus to work at all on either of our versions of eclipse,
therefore we used LucidChart and Visual Paradigm for our diagrams

*** Execution Instructions ***
Ensure you have Board.txt, Chance.txt, and CommunityChest.txt in a folder next to Monopoly.jar in 
a folder called /src
In a terminal type <java -jar Monopoly.jar> to execute the jar file

The game will run in this terminal window, enter the number of players when prompted, and the game 
will prompt you for any further inputs it requires

If the game asks you for a player name or property name, enter it into the terminal (case sensitive, 
it will display what you should type)

Enable debug mode by selecting 0 as the number of players (the game might shout at you), this can
cause some erratic behaviour and was used for testing, therefore is not meant to be fully robust
and will not do anything for features we did not use it to test

*** Data Files ***
Board.txt
Details for a square on each line, also contains key explaining the data, delimter is -
The included board is Monopoly Cork
Feel free to change the names to any version of Monopoly that you wish
	
Chance.txt
16 Lines with the descriptions of one Chance card on each line
The included chance cards are from Monopoly Cork but all are functionally identical to any version of monopoly
and the descriptions can be changed as you wish

CommunityChest.txt
16 Lines with the descriptions of one Community Chest card on each line
The included community chest cards are from Monopoly Cork but all are functionally identical to any version of monopoly
and the descriptions can be changed as you wish

Monopoly Cork cards

*** Tests ***
We felt as if the unit tests we did covered all the non trivial, testable methods in our project

*** Team Breakdown ***
We met up twice a week for 4-5 hours each session, and wrote code using a combination
of individual and pair programming.

Though we worked on everything together, each of us took the lead on certain aspects

*** Separately ***
Emma
Project structure and organisation
Jail - Logic and Interactions, Get Out of Jail Free cards
Trading
Player - Pay, Move, rolls
Bank - Auctions
Bid
Messenger - Created it, Jail
Game - Initial Input, double tracking
Unit Tests - Game, Bid, Dice

Daniel
Board - All Squares, File, and Interactions, mortgaging
Card - Card Logic, File, and Interactions
Input
Dice
Messenger - Status, Board
Player - Pay All, Score, Hand
Bank - Buying and Selling Houses
Game - Debug, Win Conditions, Bankruptcy and Scores, extra moves
Unit Tests - Board, Cards, Player
Javadoc
README

*** Collaboratively ***
Game - Initialisation and turn
Messenger - All other methods
All Diagrams