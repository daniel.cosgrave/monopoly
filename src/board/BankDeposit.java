package board;

/**The Bank Deposit Square
 * 
 *
 */
public class BankDeposit extends Square {

	private static final int DEPOSIT = 100;
	
	public BankDeposit(String name, int pos) {
		super(name, pos);
		}
	
	public int getTax() {
		return DEPOSIT;
	}

}
