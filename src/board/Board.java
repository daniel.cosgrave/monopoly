package board;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


/**Generates a board from a file containing board information, 
 * and handles inputs and outputs from it
 * 
 *
 */
public class Board{
	
	public static final int BOARD_SIZE = 40;

	private ArrayList<Square> board =  new ArrayList<Square>(); 
	
	public Board() {
		initialise();
	}

	/**Generates a list of squares from the board file by splitting them by a "-" delimiter
	 * 
	 */
public void initialise() {
		
		String line;
		String[] components;
	
		try (BufferedReader scan = new BufferedReader(new FileReader("Board.txt"))) {
			    for (int i = 0; i < BOARD_SIZE; i++) { //i should always be the position of the square
			    	line = scan.readLine();
				    components = line.split("-"); //Delimiter is a hyphen
				    switch(components[1]) {
					    case "N":
					    	isNotPurchaseable(i,components);
					    case "P":
					    	isPurchaseable(i, components);
				    }
			}
			    //Inconsistently Board.txt is not found with the first try and is found the second try
		} catch (FileNotFoundException e) {
				try (BufferedReader scan = new BufferedReader(new FileReader("src/Board.txt"))) {
				    for (int i = 0; i < BOARD_SIZE; i++) { //i should always be the position of the square
				    	line = scan.readLine();
					    components = line.split("-"); //Delimiter is a hyphen
					    switch(components[1]) {
						    case "N":
						    	isNotPurchaseable(i,components);
						    case "P":
						    	isPurchaseable(i, components);
					    }
				    }
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public ArrayList<Square> getBoard() {
		return board;
	}

	/** The process if the square to be added to the board is not purchaseable
	 * 
	 * @param i
	 * @param components
	 */
	public void isNotPurchaseable(int i, String[] components) {
		/*
    	 * J - JAIL
    	 * G - GO
		 * I - INCOME TAX
		 * B - BANK DEPOSIT
		 * C - CHANCE
		 * CC - COMMUNITY CHEST
		 * G - GO TO JAIL
		 * F - FREE PARKING
    	 */
		switch(components[2]) {
		case "J":
    		Jail j = new Jail(components[0],i);
    		board.add(j);
    		break;
		case "GO":
			GO go = new GO(components[0],i);
			board.add(go);
			break;
		case "I":
			IncomeTax tax = new IncomeTax(components[0],i);
			board.add(tax);
			break;
		case "B":
			BankDeposit dep = new BankDeposit(components[0],i);
			board.add(dep);
			break;
		case "C":
			Chance chance = new Chance(components[0],i);
			board.add(chance);
			break;
		case "CC":
			CommunityChest cc = new CommunityChest(components[0],i);
			board.add(cc);
			break;
		case "G":
			GoToJail gtj = new GoToJail(components[0],i);
			board.add(gtj);
			break;
		case "F":
			FreeParking fp = new FreeParking(components[0],i);
			board.add(fp);
			break;
		}
	}
	
	/**If the square to be added to the board is purchaseable
	 * 
	 * @param i
	 * @param components
	 */
	public void isPurchaseable(int i, String[] components) {
		/*
    	 * P - PROPERTY
		 * U - UTILITY
		 * T - TRANSPORT
    	 */
		switch(components[2]) {
			case "P":
				//Name, Position, Colour, Rents, Mortgage
				Property prop = new Property(components[0],i,Colour.valueOf(components[3]),
											 Integer.parseInt(components[4]),Integer.parseInt(components[5]), 
											 Integer.parseInt(components[6]),Integer.parseInt(components[7]), 
											 Integer.parseInt(components[8]),Integer.parseInt(components[9]), 
											 Integer.parseInt(components[10]),Integer.parseInt(components[11]), 
											 Integer.parseInt(components[12]));
				board.add(prop);
				break;
			case "U":
				Utilities util = new Utilities(components[0],i,150,75);
				board.add(util);
				break;
			case "T":
				Transport tran = new Transport(components[0],i,200,100);
				board.add(tran);
				break;
		}
	}
	
	/**Returns the set of properties belonging to a colour group
	 * 
	 * @param colour
	 * @return
	 */
	public ArrayList<Property> getPropertySet(Colour colour){
		ArrayList<Property> set = new ArrayList<Property>();
		//Loop Through Board
		for(int i = 0; i < this.getBoard().size(); i++) {
			if(this.getBoard().get(i) instanceof Property) {
				//If that square is the same colour, they are part of a set
				Colour boardSquareColour = ((Property) this.getBoard().get(i)).getColour();
					if(boardSquareColour == colour)
						set.add((Property) this.getBoard().get(i));
			}
		}
		return set;
	}

	/**returns all the utilities
	 * 
	 * @return
	 */
	public ArrayList<Utilities> getUtilitySet(){
		ArrayList<Utilities> utilities = new ArrayList<Utilities>();
		utilities.add((Utilities) this.getBoard().get(12));
		utilities.add((Utilities) this.getBoard().get(28));	
		return utilities;
	}
	
	/**Returns all the transports
	 * 
	 * @return
	 */
	public ArrayList<Transport> getTransportSet() {
		ArrayList<Transport> transports = new ArrayList<Transport>();
		transports.add((Transport) this.getBoard().get(5));
		transports.add((Transport) this.getBoard().get(15));
		transports.add((Transport) this.getBoard().get(25));
		transports.add((Transport) this.getBoard().get(35));
		return transports;
	}


	public void setBoard(ArrayList<Square> board) {
		this.board = board;
	}
	
}
