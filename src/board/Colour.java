package board;

/**Property Colours Enum
 * 
 * @author
 *
 */
public enum Colour {
		BROWN, TEAL, PINK, ORANGE, RED, YELLOW, GREEN, BLUE
}
