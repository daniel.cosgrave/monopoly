package board;

/** Income Tax Square
 *
 */
public class IncomeTax extends Square {

	private static final int TAX = 200;
	
	public IncomeTax(String name, int pos) {
		super(name, pos);
		}
	
	public int getTax() {
		return TAX;
	}

}
