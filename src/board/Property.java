package board;
import java.util.ArrayList;

/**Monopoly Property
 * 
 *
 */
public class Property extends PurchaseableSquare {
	private Colour colour;				//Each property has a colour set associated with it
	private int housePrice;				//Houses cost more on higher valued properties
	private int rent[];					//Base rent, no houses, rent with one house, two,...,hotel
	private int houses;					//The number of houses on a property
	private int hotels;					//The number of hotels on a property
	private boolean debug;

	public Property(String name, int pos, Colour colour, int price, int housePrice, //Set to file format
		int rent0, int rent1, int rent2, int rent3,
		int rent4, int rent5,int mortgage) {
		
		super(name, pos, price, mortgage);
		this.colour = colour;
		this.housePrice = housePrice; 
		this.hotels = 0;				//Initialise the number of hotels to 0
		this.houses = 0;				//Initialise the number of houses to 0
		this.rent = new int[] {rent0,rent1,rent2,rent3,rent4,rent5};
		debug = false;

	}
	
	public void buildHouse() {
		this.houses++;
	}
	
	public void buildHotel() {
		this.houses = 0;
		this.hotels++;
	}
	
	public void sellHouse() {
		this.houses--;
	}
	
	public void sellHotel() {
		this.houses = 4;
		this.hotels--;
	}
	
	/**Checks how many properties you own (and subtracts the ones that are mortgaged)
	 * 
	 * @param Set
	 * @return
	 */
	public int checkSetOwned(ArrayList<Property> Set) {
		int i;
		int numberOwned = 0;
		int numberMortgaged = 0;
		
		if(debug) {
			System.out.println("Set Colour: " + Set.get(0).getColour());
			System.out.println("Number of Properties in this set: " + Set.size());
		}
		
		for(i = 0; i < Set.size(); i++) {
			if(Set.get(i)!=null) {
				try {
					if(debug) {
						System.out.println("Index: " + i + "Property: " + Set.get(i).getName());
						System.out.println("Previously Num Mortgaged and Owned");
						System.out.println("Number Owned: " + numberOwned);
						System.out.println("Number Mortgaged: " + numberMortgaged);
					}
					
					if(Set.get(i).getOwner().equals(this.getOwner())) {
						numberOwned++;	
					}
					
					if(Set.get(i).isMortaged()) {
						numberMortgaged++;
					}
					
				}catch (NullPointerException e) {
					if(debug) {
						System.out.println("Index: " + i);
						System.out.println("Null Pointer Exception ");
					}
		        }
			}
		}

			return numberOwned - numberMortgaged;
	}
	
	/**
	 * Returns true if the player also owns the other properties in the set
	 * *Test this*
	 * @param Set
	 * @return
	 */
	public boolean checkWholeSetOwned(ArrayList<Property> Set, int numberOwned) {
		if(numberOwned==Set.size()) {
			return true;
		}
		else return false;
	}
	
	public int getHousePrice() {
		return housePrice;
	}
	
	public int getRent(int index) {
		return rent[index - 1];
	}
	
	public int getHouses() {
		return houses;
	}
	
	public void setHouses(int houses) {
		this.houses = houses;
	}
	
	public int getHotels() {
		return hotels;
	}
	
	public Colour getColour() {
		return colour;
	}

	public void setColour(Colour colour) {
		this.colour = colour;
	}

	public void setHotels(int hotels) {
		this.hotels = hotels;
	}

}
