package board;
import player.Player;

/**Any purchaseable square in monopoly
 * 
 *
 */
public class PurchaseableSquare extends Square {
	private int price;
	private int mortgage;
	private Player owner;
	private boolean owned;
	private boolean mortgaged;
	
	public PurchaseableSquare(String name, int pos, int price, int mortgage) {
		super(name,pos);
		this.price = price;
		this.mortgage = mortgage;
		this.owned = false;
		this.mortgaged = false;
	}
	
	public  int getPrice() {
		return this.price;
	}
	
	public void setOwner(Player p) {
		this.owner = p;
		this.owned = true;
	}
	
	public boolean isOwned() {
		return owned;
	}
	
	public Player getOwner () {
	    return owner;
	}
	
	public  int getMortgage() {
		return this.mortgage;
	}

	public boolean isMortaged() {
		return mortgaged;
	}

	public void setMortaged(boolean mortaged) {
		this.mortgaged = mortaged;
	}

	public boolean isMortgaged() {
		return mortgaged;
	}


	
}
