package board;

/**An abstract square with base features
 * 
 *
 */
public abstract class Square {
	private String name;
	private int position;
	
	public Square(String name, int position) {
		this.name = name;
		this.position = position;
	}
	
	public String getName() {
		return this.name;
		
	}
	
	public  int getPosition() {
		return this.position;
	}
}
