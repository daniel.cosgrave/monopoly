package board;

/**Transport (Railroad) squares
 * 
 * @author 1
 *
 */
public class Transport extends PurchaseableSquare {

	private int[] rent = new int[] {25, 50, 100, 200};
	public Transport(String name, int pos, int price, int mortgage) {
		super(name, pos, price, mortgage);
		}
	
	/**
	 * 
	 * @param index
	 * @return
	 */
	public int getRent(int index) {
		return rent[index-1];
	}
}
