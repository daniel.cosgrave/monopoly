package board;

/**Utility Squares
 * 
 *
 */
public class Utilities extends PurchaseableSquare {
	
	public Utilities(String name, int pos, int price, int mortgage) {
		super(name, pos, price, mortgage);
		}
	
	/**
	 * The rent for landing on a utility is charged based on the number of other utilities owned
	 * If they own 1 the number rolled by the dice is multiplied by 4
	 * If they own 2 the number rolled by the dice is multiplied by 10
	 * @param utilitiesOwned
	 * @return rent factor
	 */
	private int getRentFactor(int utilitiesOwned) {
		switch(utilitiesOwned){
		case 1:
			return 4;
		case 2:
			return 10;
		default: 
			return 0;
		}
	}
	
	/**
	 * Replaces the getRent method of its superclass, instead it returns the rent factor multiplied by
	 * the dice roll
	 * @param utilitiesOwned
	 * @param diceRoll
	 * @return rent
	 */
	 public int getRent(int utilitiesOwned, int diceRoll)
	   {
	      return getRentFactor(utilitiesOwned) * diceRoll;
	   }
}
