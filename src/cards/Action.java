package cards;

/**Types of actions cards can take
 * 
 *
 */
public enum Action {
	BANK, PLAYER, MOVE, MOVE_TO,
	MOVE_NEAREST, REPAIRS, OUT_JAIL
}
