package cards;

/** Card Interface, ties together chance and community chest
 * 
 *
 */
public interface Card {
	
	public void action(int cardNumber);
	public String search(int cardNumber);
	public String getDetails();
}
