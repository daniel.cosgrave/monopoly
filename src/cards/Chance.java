package cards;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import cards.Action;

/** Chance card implementation
 * 
 *
 */
public class Chance implements Card{
	
	private String cardDetails;
	
	private Action action;
	private int amount;
	private int move;
	private int moveTo;
	private boolean getOutOfJailFreeCard = false;
	private int houseCost;
	private int hotelCost;
	private boolean nearestUtilities = false;
	private boolean jailFlag = false;
	

	public Chance(int cardNumber) {
		cardDetails = search(cardNumber);
		action(cardNumber);
	}
	
	/*Card Action Switch Statement*/

	/**
	 * 16 Types of Chance Cards
	 * @param cardNumber
	 */
	public void action(int cardNumber){
		switch(cardNumber) {
			case 0:
				getOutOfJail();
				break;
			case 1:
				advanceToRaceCourse();
				break;
			case 2:
				advanceToTran();
				break;
			case 3:
				donate();
				break;
			case 4:
				advanceToFota();
				break;
			case 5:
				advanceToBlar();
				break;
			case 6:
				chairman();
				break;
			case 7:
				goBackThree();
				break;
			case 8:
				repairs();
				break;
			case 9:
				golf();
				break;
			case 10:
				advanceToGO();
				break;
			case 11:
				bill();
				break;
			case 12:
				advanceToUtil();
				break;
			case 13:
				advanceToKent();
				break;
			case 14:
				goToJail();
				break;
			case 15:
				advanceToTran();
				break;
		}
	}
	
	/*Card Action Methods*/
	
	private void getOutOfJail() {
		action = Action.OUT_JAIL;
		getOutOfJailFreeCard = true;
	}

	private void advanceToRaceCourse() {
		action = Action.MOVE_TO;
		moveTo = 11;
	}
	
	private void goToJail() {
		action = Action.MOVE_TO;
		jailFlag = true;
	}

	private void advanceToKent() {
		action = Action.MOVE_TO;
		moveTo = 5;
	}

	/**
	 * Advance to the nearest utility and if it is owned pay double rent
	 */
	private void advanceToUtil() {
		action = Action.MOVE_NEAREST;
		nearestUtilities = true;	
	}

	private void bill() {
		action = Action.BANK;
		amount = 15;
	}

	private void advanceToGO() {
		action = Action.MOVE_TO;
		moveTo = 0;
	}

	private void golf() {
		action = Action.BANK;
		amount = -50;	
	}

	private void repairs() {
		//FOR EACH HOUSE PAY M25 AND FOR EACH HOTEL PAY M100
		action = Action.REPAIRS;
		houseCost = 25;
		hotelCost = 100;
	}

	private void goBackThree() {
		action = Action.MOVE;
		move = -3;
	}

	private void chairman() {
		action = Action.PLAYER;
		amount = 50;
	}

	private void advanceToBlar() {
		action = Action.MOVE_TO;
		moveTo = 39;
	}

	private void advanceToFota() {
		action = Action.MOVE_TO;
		moveTo = 24;
	}

	private void donate() {
		action = Action.BANK;
		amount = -150;
		
	}

	private void advanceToTran() {
		action = Action.MOVE_NEAREST;
		nearestUtilities = false;
	}

	/*Card Text Search*/
	
	/** Returns a string by searching for a card's corresponding index in the file
	 * @param cardNumber
	 * @return String
	 */
	public String search(int cardNumber) {
		
		try (BufferedReader scan = new BufferedReader(new FileReader("Chance.txt"))) {
			if(cardNumber >= 0 && cardNumber <= 15) {
			    for (int i = 0; i < cardNumber; i++)
			        scan.readLine();
			    return scan.readLine();
			}
			 else
				 return null;
		} catch (FileNotFoundException e) {
			try (BufferedReader scan = new BufferedReader(new FileReader("src/Chance.txt"))) {
				if(cardNumber >= 0 && cardNumber <= 15) {
				    for (int i = 0; i < cardNumber; i++)
				        scan.readLine();
				    return scan.readLine();
				}
				 else
					 return null;
			} catch (FileNotFoundException e1) {
				e.printStackTrace();
			} catch (IOException e1) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	/*Getters and Setters*/

	public Action getAction() {
		return action;
	}


	public int getAmount() {
		return amount;
	}


	public int getMove() {
		return move;
	}


	public int getMoveTo() {
		return moveTo;
	}


	public boolean isGetOutOfJailFreeCard() {
		return getOutOfJailFreeCard;
	}


	public int getHouseCost() {
		return houseCost;
	}


	public int getHotelCost() {
		return hotelCost;
	}


	public boolean isNearestUtilities() {
		return nearestUtilities;
	}


	public boolean isJailFlag() {
		return jailFlag;
	}
	
	public String getDetails(){
		return cardDetails;
	}

	public String getCardDetails() {
		return cardDetails;
	}

	public void setCardDetails(String cardDetails) {
		this.cardDetails = cardDetails;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public void setMove(int move) {
		this.move = move;
	}

	public void setMoveTo(int moveTo) {
		this.moveTo = moveTo;
	}

	public void setGetOutOfJailFreeCard(boolean getOutOfJailFreeCard) {
		this.getOutOfJailFreeCard = getOutOfJailFreeCard;
	}

	public void setHouseCost(int houseCost) {
		this.houseCost = houseCost;
	}

	public void setHotelCost(int hotelCost) {
		this.hotelCost = hotelCost;
	}

	public void setNearestUtilities(boolean nearestUtilities) {
		this.nearestUtilities = nearestUtilities;
	}

	public void setJailFlag(boolean jailFlag) {
		this.jailFlag = jailFlag;
	}
	
}
