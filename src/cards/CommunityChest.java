package cards;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import cards.Action;

/**Community Chest Card Implementation
 * 
 *
 */
public class CommunityChest implements Card {

	private String cardDetails;
	private Action action;
	private int amount;
	private int moveTo;
	private boolean getOutOfJailFreeCard;
	private int houseCost;
	private int hotelCost;
	private boolean jailFlag;
	
	public CommunityChest(int cardNumber) {
		cardDetails = search(cardNumber);
		getOutOfJailFreeCard = false;
		action(cardNumber);
		jailFlag = false;
	}
	
	/*Card Action Switch Statement*/

	/**
	 * 16 Types of Community Chest Cards
	 * @param cardNumber
	 */
	public void action(int cardNumber){
		switch(cardNumber) {
			case 0:
				getOutOfJailFree();
				break;
			case 1:
				subscription();
				break;
			case 2:
				birthday();
				break;
			case 3:
				rugby();
				break;
			case 4:
				familyDayOut();
				break;
			case 5:
				seals();
				break;
			case 6:
				goToJail();
				break;
			case 7:
				repairs();
				break;
			case 8:
				blarneyVisit();
				break;
			case 9:
				embarrassed();
				break;
			case 10:
				scholarship();
				break;
			case 11:
				littering();
				break;
			case 12:
				advanceToGo();
				break;
			case 13:
				ringTheBells();
				break;
			case 14:
				museum();
				break;
			case 15:
				cruise();
				break;
		}
	}

	/*Card Action Methods*/
	
	private void cruise() {
		action = Action.BANK;
		amount = 50;
	}

	private void museum() {
		action = Action.BANK;
		amount = -10;
	}

	private void ringTheBells() {
		action = Action.BANK;
		amount = -25;
	}

	private void advanceToGo() {
		action = Action.MOVE_TO;
		moveTo = 0;
		
	}

	private void littering() {
		action = Action.BANK;
		amount = 100;
		
	}

	private void scholarship() {
		action = Action.BANK;
		amount = -100;
		
	}

	private void embarrassed() {
		action = Action.BANK;
		amount = 50;
	}

	private void blarneyVisit() {
		action = Action.BANK;
		amount = -200;	
	}

	private void repairs() {
		action = Action.REPAIRS;
		houseCost = 40;
		hotelCost = 115;	
	}

	private void goToJail() {
		action = Action.MOVE_TO;
		jailFlag = true;
	}

	private void getOutOfJailFree() {
		action = Action.OUT_JAIL;
		getOutOfJailFreeCard = true;
	}
	
	private void familyDayOut() {
		action = Action.BANK;
		amount = -100;
	}

	private void rugby() {
		action = Action.BANK;
		amount = -100;
	}

	private void birthday() {
		action = Action.PLAYER;
		amount = -10;
	}

	private void subscription() {
		action = Action.BANK;
		amount = -50;
		
	}

	private void seals() {
		action = Action.BANK;
		amount = -20;
		
	}

	/*Card Text Search*/

public String search(int cardNumber) {
		
		try (BufferedReader scan = new BufferedReader(new FileReader("CommunityChest.txt"))) {
			if(cardNumber >= 0 && cardNumber <= 15) {
			    for (int i = 0; i < cardNumber; i++)
			        scan.readLine();
			    return scan.readLine();
			}
			 else
				 return null;
		} catch (FileNotFoundException e) {
			try (BufferedReader scan = new BufferedReader(new FileReader("src/CommunityChest.txt"))) {
				if(cardNumber >= 0 && cardNumber <= 15) {
				    for (int i = 0; i < cardNumber; i++)
				        scan.readLine();
				    return scan.readLine();
				}
				 else
					 return null;
			} catch (FileNotFoundException e1) {
				e.printStackTrace();
			} catch (IOException e1) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
		
	}

	/*Getters and Setters*/

	public String getDetails() {
		return cardDetails;
	}
	
	public Action getAction() {
		return action;
	}
	
	
	public int getAmount() {
		return amount;
	}
	
	public int getMoveTo() {
		return moveTo;
	}
	
	
	public boolean isGetOutOfJailFreeCard() {
		return getOutOfJailFreeCard;
	}
	
	
	public int getHouseCost() {
		return houseCost;
	}
	
	
	public int getHotelCost() {
		return hotelCost;
	}

	public boolean isJailFlag() {
		return jailFlag;
	}

	public String getCardDetails() {
		return cardDetails;
	}

	public void setCardDetails(String cardDetails) {
		this.cardDetails = cardDetails;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public void setMoveTo(int moveTo) {
		this.moveTo = moveTo;
	}

	public void setGetOutOfJailFreeCard(boolean getOutOfJailFreeCard) {
		this.getOutOfJailFreeCard = getOutOfJailFreeCard;
	}

	public void setHouseCost(int houseCost) {
		this.houseCost = houseCost;
	}

	public void setHotelCost(int hotelCost) {
		this.hotelCost = hotelCost;
	}

	public void setJailFlag(boolean jailFlag) {
		this.jailFlag = jailFlag;
	}
	
}