package cards;

import java.util.ArrayList;
import java.util.Collections;

/**Builds a deck of either card type
 * 
 * @author 1
 *
 */
public class Deck {
	
	public static final int DECKLENGTH = 16;
	
	private CardType deckType;
	private ArrayList<Integer> deck = new ArrayList<Integer>();
	
	/**Generates an ArrayList of indexed card numbers from 0 to 16
	 * These Correspond to a list of cards in text files which can be searched when drawn
	 * @param type
	 */
	   public Deck (CardType type) {
	      deckType = type;
	      
	      //Generate linearly spaced arraylist of cardnumbers
	      for (int i = 0; i < DECKLENGTH; ++i) {
	          deck.add(i);
	      }
	      
	    }
	   
	  /**
	    * Drawing a card from the deck
	    * Get the cardNumber, then remove it from the deck, if it's not a get out of jail free card
	    * return it to the bottom
	    * @return card
	    */
	   public int drawCard(){
	      int card = deck.get(0);
	      deck.remove(0);
	      switch(card) {
	      case 0: 
	    	//Special Case, get out of jail free, does not get returned to the deck
	      default:
	    	  moveToBottom(card);
	      }
	      return card;
	   }
	   
	   /**Shuffles the deck
	    * Uses collections.shuffle
	    */
	   public void shuffle() {
		   Collections.shuffle(deck);
	   }
	   
	   /**Returns the type of deck (chance or community chest)
	    * 
	    * @return deckType
	    */
		public CardType getDeckType() {
			return deckType;
		}
		
		/** Puts a card at the bottom of the deck
		 * 
		 * @param cardNumber
		 */
		public void moveToBottom(int cardNumber) {
			//Deck should only ever draw from the top so index to remove should always be 0
			deck.add(cardNumber);
		}

		public ArrayList<Integer> getDeck() {
			return deck;
		}

		public void setDeck(ArrayList<Integer> deck) {
			this.deck = deck;
		}


		public void setDeckType(CardType deckType) {
			this.deckType = deckType;
		}
	
}
 