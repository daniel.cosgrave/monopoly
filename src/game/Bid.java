package game;

import java.util.ArrayList;
import board.PurchaseableSquare;
import player.Player;
import game.Messenger;

/** Bid class for auctions
 * 
 *
 */
public class Bid {
	private int amount;
	private Player bidder;
	private Input playerInput;
	
	public Bid(){
		amount = 0;
		playerInput = new Input();
	}
	
	public int getAmount(){
		return amount;
	}
	
	public Player getBidder(){
		return bidder;
	}
	
	public void setBid(int newAmount, Player newBidder) {
		this.amount = newAmount;
		this.bidder = newBidder;
	}
	
	/** Gets a new bid with input from the player
	 * 
	 * @param psq
	 * @param players
	 * @param debug
	 * @param currentBid
	 * @return whether a bid was exited or not
	 */
	public boolean getBid(PurchaseableSquare psq, ArrayList<Player> players, boolean debug, Bid currentBid) {
		Messenger messenger = new Messenger();
		
		messenger.printAuction(players,psq);
		messenger.printPlayers(players);
		
		int i=0;
		
		boolean keepGoing = true;
		boolean isValid = false;
		
		//Bid auction = new Bid();
		
		while(keepGoing) {
			
			playerInput.getPlayerName(players, debug);
			isValid = playerInput.isValid();
			keepGoing = playerInput.isKeepGoing();
			i = playerInput.getIndex();
			
			if(!isValid) {
				messenger.printInvalidInput();
			}
			
			else if (keepGoing) {
				messenger.printGetBid(players.get(i).getName(),currentBid);
				int bidAmount = playerInput.getNumber();
		    	this.setBid(bidAmount, players.get(i));
		    	keepGoing = false;
			}
			else
				isValid = false;
			
		}
		return isValid;
	}
	/*
	public void completeBid(Bid bid, PurchaseableSquare psq, Player player, Bank bank) {
		player.pay(bank, bid.getAmount());
		psq.setOwner(player);
	}*/

	public Input getPlayerInput() {
		return playerInput;
	}

	public void setPlayerInput(Input playerInput) {
		this.playerInput = playerInput;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public void setBidder(Player bidder) {
		this.bidder = bidder;
	}
}

