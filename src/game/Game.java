package game;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import board.*;
import cards.*;
import cards.CardType;
import player.*;


/** Central gameplay features
 *
 *
 */
public class Game {
	ArrayList<Player> players;  //A list containing every player still in the game
	Board MonopolyBoard;		//A list containing every square and all of their properties
	Deck chanceDeck;			//Deck of chance cards
	Deck communityChestDeck;	//Deck of community chest cards
	int rentMultiplier;			//Certain cards can influence the rent price of a property
	Messenger messenger;		//The method of communication the with the user
	Bank bank;					//The bank which contains the number of houses		
	int numberOfPlayers;		//The number of players initially in the game
	int lastRoll = 0;			//The most recent roll
	Input playerInput;			//Handles how players interact with the game
	ArrayList<String> tokens;	//The different tokens in the game
	boolean debug;				//Debug flag, outputs debug info to system.out
	boolean hasNoJailFreeCard=true; //Used in jail situations
	int bankruptcyLimit;		//Max amount of bankruptcies before the game ends (by default 2)
	int bankruptcies;			//The amount of bankruptcies recorded

	//Constructor 
	public Game() {
		MonopolyBoard = new Board();
		chanceDeck = new Deck(CardType.CHANCE);
		communityChestDeck = new Deck(CardType.COMMUNITY);
		rentMultiplier = 1;
		messenger = new Messenger();
		bank = new Bank();
		numberOfPlayers = 0;
		players = new ArrayList<Player>();
		tokens = new ArrayList<String>();
		tokens.add("Top-Hat");
		tokens.add("Cannon");
		tokens.add("Dog");
		tokens.add("Battleship");
		tokens.add("Boot");
		tokens.add("Iron");
		playerInput = new Input();
		debug = false;
		bankruptcyLimit = 2;
	}
	
    public Game(ByteArrayInputStream in) {
    	System.setIn(in);
 		MonopolyBoard = new Board();
 		chanceDeck = new Deck(CardType.CHANCE);
 		communityChestDeck = new Deck(CardType.COMMUNITY);
 		rentMultiplier = 1;
 		messenger = new Messenger();
 		bank = new Bank();
 		numberOfPlayers = 0;
 		players = new ArrayList<Player>();
 		tokens = new ArrayList<String>();
 		tokens.add("Top-Hat");
 		tokens.add("Cannon");
 		tokens.add("Dog");
 		tokens.add("Battleship");
 		tokens.add("Boot");
 		tokens.add("Iron");
 		playerInput = new Input();
 		debug = false;
 		bankruptcyLimit = 2;
    }

	
	/**
	 * run() handles the background game play and user interface
	 */
	public void run() {

		/**************************
		 * Initialising Decks, Number of Players
		 *************************/
		communityChestDeck.shuffle();
		chanceDeck.shuffle();

    	boolean wrongNumPlayers=true;   	 //Starting Number of Players
    	boolean repeat = false;
    	//Get a number of player between 2 and 6
    	while(wrongNumPlayers) {
    		numberOfPlayers = playerInput.getNumPlayers(repeat);
    		if(numberOfPlayers == 0) {
    			System.out.println("Enable debug mode? (y/n)");
    			debug = playerInput.getYesOrNo();
    		}
    		
    		//Debug allows you to have any number of players... use this cautiously
    		if(repeat && debug)
    			wrongNumPlayers = false;
    		
	    	if (numberOfPlayers<=6 && numberOfPlayers >=2) {
	    		wrongNumPlayers=false;
	    	}
	    	repeat = true;
    	}
    	
    	/*Ask for input on who is playing and create the player list*/
		players = playerInput.createPlayerList(tokens, numberOfPlayers, debug);
	
	/****************************************
	 * Main Game flow
	 ***************************************/
			
		/*Special case of a 2 player game where there can only be 1 bankruptcy before the game ends*/
		if(numberOfPlayers==2) {
			bankruptcyLimit = 1;
		}

		int turns = 0; //Turn Counter for debugging
		
		while (bankruptcies < bankruptcyLimit) {
			for(int i=0;i<players.size();i++) {
				extraMoves();
				
				if(debug) {
					System.out.println("The current player is " + players.get(i).getName());
					System.out.println("The number of bankruptices is " + bankruptcies + " Bankruptcy limit (" + bankruptcyLimit + ")");
					System.out.println("Turn: " + turns);
				}
				
				messenger.turnHeader(players.get(i));
				if(turn(players.get(i))) {
					bankruptcies++;
					players.remove(players.get(i));
				}
				turns++;
			}
		}
		
		ArrayList<Player> winners = tallyScore();
		messenger.printFinalScores(players, winners);
		
	}

	/**Handles the current player's turn and extra moves
	 * 
	 * @param currentPlayer
	 */
	private boolean turn(Player currentPlayer) {
		int fine = 50;
		boolean wentBankrupt = false;
		Messenger messenger = new Messenger();
		int decision;
		/*======== Taking a Normal Turn ========*/
		if (!currentPlayer.isInJail()) {
			/*Rolling and Moving*/
			int[] roll=currentPlayer.twoDiceRoll();
			currentPlayer.movePlayer(roll);
			if(debug)
				System.out.println("\nDouble count is "+currentPlayer.getDoubleCount());
	
			messenger.printPlayerPosition(currentPlayer,MonopolyBoard);
			Square landedOnSquare = MonopolyBoard.getBoard().get(currentPlayer.getPosition());
			
			/*Interacting with the current square*/
			if(squareInteractions(currentPlayer, landedOnSquare))
				wentBankrupt = true;
			lastRoll = roll[0] + roll [1]; //Track the last roll for utility features
			
			while(currentPlayer.getDoubleCount()>0 && !wentBankrupt) {
				turn(currentPlayer);
			}
			/*Extra Moves to be taken*/
			//extraMoves();
		}
		
		/*======== In Jail and Getting Out ========*/
		
		else if(currentPlayer.getEscapingJail()>0){
			int[] roll= currentPlayer.twoDiceRoll();
			if(roll[0]==roll[1]) {
				currentPlayer.getOutOfJail();
			}
			else {
				int n =currentPlayer.getEscapingJail();
				currentPlayer.setEscapingJail(n-1);
				if(currentPlayer.getEscapingJail()==0) {
					if(!currentPlayer.pay(bank, fine)) {
						if(bankruptcyProcess(currentPlayer, fine)){
							return wentBankrupt;
						}
					}
					currentPlayer.getOutOfJail();
				}
			}
		}

		/*They are in Jail and did not get out*/
		else { 		
			
			while (hasNoJailFreeCard) {
				hasNoJailFreeCard=false; // so you will exit the loop unless changed

		    	messenger.printJailOptions();
		    	decision = playerInput.getNumber();
		    	
				switch (decision) {
					case 0:
						int[]roll=currentPlayer.twoDiceRoll();
						if(roll[0]==roll[1]) {
							currentPlayer.getOutOfJail();
							break;
						}
						
						else {
							currentPlayer.setEscapingJail(2);
							break;
						}
						
					case 1:
						this.Trade(currentPlayer, players);
						if(currentPlayer.useJailFreeCard()) {
							currentPlayer.getOutOfJail();
							break;
						}
						else {
							hasNoJailFreeCard=true; // go back and pick a new option
							break;
						}
						
					case 2:
						currentPlayer.pay(bank, fine);
						currentPlayer.getOutOfJail();
						break;
					case 3:
						if(currentPlayer.useJailFreeCard()) {
							currentPlayer.getOutOfJail();
							break;
						}
						else {
							hasNoJailFreeCard=true; // go back and pick a new option
							break;
						}
						
					default:
						messenger.printInvalidInput();
						break;
				}
			}
	    	//currentPlayer.getOutOfJail();
		}
		hasNoJailFreeCard=true;
		return wentBankrupt;
	}
	
	/**All the options a player can take outside of their turn
	 * 
	 */
	private void extraMoves() {
		boolean keepGoing = true;
		while (keepGoing) {
			/*Ask if anyone wants to do anything extra*/
			messenger.keepGoingPrompt();
			messenger.printPlayers(players);
			boolean isValid = false;
			Player currentPlayer;
			int i;
			
			/*Asking what player (or none) wants to take the action and validating it*/
			playerInput.getPlayerName(players, debug);
			isValid = playerInput.isValid();
			keepGoing = playerInput.isKeepGoing();
			i = playerInput.getIndex();
			
			if(!isValid) {
				messenger.printInvalidInput();
			}
			
			else if (keepGoing) {
				messenger.printKeepGoing();
				currentPlayer = players.get(i);
				int decision = playerInput.getNumber();
				switch (decision) {
					case 0:
						//Do Nothing
						break;
					case 1:
						//Build
						if(bank.buyHH(players.get(i),MonopolyBoard))
							messenger.printBuilt();
						break;
					case 2:
						//Sell
						if(bank.sellHH(players.get(i),MonopolyBoard))
							messenger.printSold();
						break;
					case 3:
						//Mortgage
						mortgage(currentPlayer);
						break;
					case 4:
						//Trade
						this.Trade(players.get(i), players);
						break;
					case 5:
						messenger.printBoard(MonopolyBoard, players, bank);
						break;
					case 6:
						messenger.printStatus(currentPlayer,MonopolyBoard);
						break;
					default:
						messenger.printInvalidInput();
						break;
				}
			}
		}
	}

	/**Mortgage a Property, or Unmortgage it
	 * 
	 * @param player
	 */
	private void mortgage(Player player) {
		boolean isValid = false;
		boolean keepGoing = true;
		int i = 0;
		
		messenger.printHand(player);
		//Add press x to exit in messenger
		while(keepGoing) {
			if(player.getHand().size() == 0 ) {
				messenger.printEmptyHand();
				keepGoing = false;
				break;
			} 
			
			playerInput.getSquareName(player.getHand());
			isValid = playerInput.isValid();
			keepGoing = playerInput.isKeepGoing();
			i = playerInput.getIndex();
			
			if(!isValid) {
				messenger.printInvalidInput();
			}
			
			else if (keepGoing) {

				
				if(player.getHand().get(i).isMortaged()) {
					double mortgageValue = player.getHand().get(i).getMortgage() + player.getHand().get(i).getMortgage()*0.1;
					if(player.pay(bank, (int) mortgageValue)) {
						player.getHand().get(i).setMortaged(false);
						messenger.printMortgaged(player.getHand().get(i));
						keepGoing = false;
					}
					else {
						messenger.printNotEnoughMoney();
					}
				}
				else {
					bank.pay(player, player.getHand().get(i).getMortgage());
					player.getHand().get(i).setMortaged(true);
					messenger.printMortgaged(player.getHand().get(i));
					keepGoing = false;
				}
			}	
		}
	}
		


	/**Trade things in hand with another player
	 * 
	 * @param p
	 * @param players
	 */
	public void Trade(Player p, ArrayList<Player> players) {

		boolean keepGoing = true;
		while (keepGoing) {
			/*Ask if anyone wants to do anything extra*/
			messenger.printTradeWith(p);
			messenger.printPlayersExcept(players,p);
			boolean isValid = false;
			Player currentPlayer;
			int i;
			
			/*Asking what player (or none) wants to take the action and validating it*/
			playerInput.getPlayerNameExcept(players, debug, p);
			isValid = playerInput.isValid();
			keepGoing = playerInput.isKeepGoing();
			i = playerInput.getIndex();

			
			if(!isValid) {
				messenger.printInvalidInput();
			}
			
			else if (keepGoing) {
				messenger.printTradeWhat();
				currentPlayer = players.get(i);
				int decision = playerInput.getNumber();
								
				switch (decision) {
					case 0:
						//Do Nothing
						break;
					case 1:
						//Get out jail card
						boolean noAgreement = true;
						int offer1=0;
						int proposition1=0;
						
						if(currentPlayer.getJailFreeCards()<=0) {
							messenger.printNoJailCard();
						}
						
						else {
						keepGoing=false;
					
							while(noAgreement) {
								messenger.printGetTradeOffer(currentPlayer.getName());
								offer1=playerInput.getNumberOrExit();
								if(offer1 == -1) {
									break;
								}
								messenger.printGetTradeProposition(p.getName());
								proposition1=playerInput.getNumberOrExit();
								if(proposition1 == -1) {
									break;
								}
								else if(offer1==proposition1) {
									noAgreement=false;
									currentPlayer.useJailFreeCard();
									p.aquiredJailFreeCard();
									p.pay(currentPlayer, offer1);
									messenger.printBoughtJailCard(p, currentPlayer, offer1);
								}
							}
						}
									
						
						break;
					case 2:
						//Purchasable square
						boolean noAgree = true;
						int offer=0;
						int proposition=0;
						
						ArrayList<PurchaseableSquare> hand = p.getHand();

						if(hand.size()==0) {
							messenger.printEmptyHand();
						}
						
											
						else {
							
							boolean wrongSpelling=true;
							int handi=0;
							
							
							while (wrongSpelling) {
								wrongSpelling=false;
								/*Get which square*/
								messenger.printTradeWhich();
								messenger.printHand(p);
								playerInput.getSquareName(hand);
								handi = playerInput.getIndex();
								if(debug) {
									System.out.println("Index is: " + handi);
								}

								if(handi>=hand.size()) {
									wrongSpelling=true;
									messenger.printInvalidInput();
								}
									
							}

							String squareName = hand.get(handi).getName();
							
							while(noAgree) {
								
								messenger.printGetTradeOffer(currentPlayer.getName());
								offer=playerInput.getNumberOrExit();
								
								if(offer == -1) {
									break;
								}
								
								messenger.printGetTradeProposition(p.getName());
								proposition=playerInput.getNumberOrExit();
								
								if(proposition == -1) {
									break;
								}
								
								else if(offer==proposition) {
									noAgree=false;
								}
							}
							
							if (offer == -1) {
								
							}
							else if(p.sellProperty(players.get(i), hand.get(handi), offer)) {
								messenger.bought(players.get(i), squareName);
							
							}
							
							else {
								messenger.printNotEnoughMoney();	
							}
							keepGoing=false;
						}
						break;
				
					default:
						messenger.printInvalidInput();
						break;
				}
			}
		}


	}
	/**Checks if a square is owned by another player
	 * 
	 * @param player
	 * @param sq
	 * @return 
	 */
	public boolean squareInteractions(Player player, Square sq) {
		Messenger messenger = new Messenger();
		boolean wentBankrupt = false;
		if (sq instanceof PurchaseableSquare){
			PurchaseableSquare psq = (PurchaseableSquare) sq;
			boolean owned = psq.isOwned();
			//Offer to buy if not owned or mortgaged
			if(!owned) {
				tryToBuy(player,psq,owned);
			}
				
			else if(psq.isMortaged()) {
				messenger.printIsMortgaged();
			}
				
			else if(owned)
				if(chargeRent(player,psq))
					wentBankrupt = true;
		}
		
		else if (sq instanceof IncomeTax) {
			IncomeTax inc = (IncomeTax) sq;
			messenger.printTax(player);
					if(!player.pay(bank, inc.getTax())) {
						if(bankruptcyProcess(player, inc.getTax()))
							wentBankrupt = true;
						else 
							player.pay(bank, inc.getTax());
					}		
		}
		
		else if (sq instanceof BankDeposit) {
			BankDeposit dep = (BankDeposit) sq;
			if(!player.pay(bank, dep.getTax())) {
				if(bankruptcyProcess(player, dep.getTax()))
					wentBankrupt = true;
				else 
					player.pay(bank, dep.getTax());
			}	
		}
		else if(sq instanceof GoToJail) {
			player.GoToJail();
		}
		else if(sq instanceof board.Chance) {
			if(chanceInteractions(player))
				wentBankrupt = true;
		}
		else if(sq instanceof board.CommunityChest) {
			if(communityChestInteractions(player))
				wentBankrupt = true;
		}
		return wentBankrupt;
	}

	/**Handles all the interactions between the players and chance cards
	 * 
	 * @param player
	 * @return wentBankrupt
	 */
	public boolean chanceInteractions(Player player) {
		int cardNumber = chanceDeck.drawCard();
		cards.Chance chance = new cards.Chance(cardNumber);
		Square landedOnSquare;
		messenger.printCardAction(chance.getDetails());
		boolean wentBankrupt = false;
		
		if(debug)
			System.out.println("CardNumber: " + cardNumber);
		Action action = chance.getAction();
		//Some cards can change the amount of rent you pay
		switch (action) {
		case BANK:
			if(!player.pay(bank, chance.getAmount()) && chance.getAmount() > 0) {
					if(bankruptcyProcess(player, chance.getAmount()))
						wentBankrupt = true;
					else
						player.pay(bank, chance.getAmount());
			}
			break;
		case MOVE:
			player.movePlayer(chance.getMove());
			landedOnSquare = MonopolyBoard.getBoard().get(player.getPosition());
			squareInteractions(player, landedOnSquare);
			break;
		case MOVE_NEAREST:
			rentMultiplier = 2;
			int [] squares;
			if(chance.isNearestUtilities()) {
				squares = new int[] {12,28};
			}
			else {
				squares = new int[] {5,15,25,35};
			}
			int closest = closestTo(player.getPosition(),squares);
			player.goTo(closest);	
			landedOnSquare = MonopolyBoard.getBoard().get(player.getPosition());
			if(squareInteractions(player, landedOnSquare))
				wentBankrupt = true;
			rentMultiplier = 1;
			break;
		case MOVE_TO:
			if(chance.isJailFlag()) 
				player.GoToJail();
			else {
				player.goTo(chance.getMoveTo());
				landedOnSquare = MonopolyBoard.getBoard().get(player.getPosition());
				if(squareInteractions(player, landedOnSquare))
					wentBankrupt = true;
			}
			break;
		case OUT_JAIL:
			player.aquiredJailFreeCard();
			break;
		case PLAYER:
			if(player.payAll(players, chance.getAmount())) {
				break;
			}
			else {
				wentBankrupt = true;
			}
			break;
		case REPAIRS:
			int repairSum = payRepairs(chance.getHouseCost(),chance.getHotelCost(),player);
			if(player.pay(bank, repairSum))
				break;
			else 
				wentBankrupt = true;
			break;
		
		}
		
		return wentBankrupt;
	}

	/**All the possible interactions from community chest cards
	 * 
	 * @param player
	 * @return 
	 */
	public boolean communityChestInteractions(Player player) {
		int cardNumber = communityChestDeck.drawCard();
		cards.CommunityChest communityChest = new cards.CommunityChest(cardNumber);
		boolean wentBankrupt = false;
		messenger.printCardAction(communityChest.getDetails());
		if(debug)
			System.out.println("CardNumber: " + cardNumber);
		
		Action action = communityChest.getAction();
		//Some cards can change the amount of rent you pay
		switch (action) {
		case BANK:
			if(!player.pay(bank, communityChest.getAmount()) && communityChest.getAmount() > 0) {
				if(bankruptcyProcess(player, communityChest.getAmount()))
					wentBankrupt = true;
				else
					player.pay(bank, communityChest.getAmount());
		}
			break;
		case MOVE_TO:
			if(communityChest.isJailFlag()) {
				player.GoToJail();
			}
			else {
				player.goTo(communityChest.getMoveTo());
				Square landedOnSquare = MonopolyBoard.getBoard().get(player.getPosition());
				squareInteractions(player, landedOnSquare);
			}
			break;
		case OUT_JAIL:
			player.aquiredJailFreeCard();
			break;
		case PLAYER:
			for(int i = 0; i < players.size(); i++) {
				if(!players.get(i).equals(player)) {
					if(!players.get(i).pay(player, -communityChest.getAmount())) {
						if(bankruptcyProcess(players.get(i), communityChest.getAmount())) {
							bankruptcies++;
							
							//if(debug) {
								System.out.println("Went to this point " + players.get(i).getName());
							//}
							
							players.remove(players.get(i));	
					}
					}
				}
			}	
			//Go into bankruptcy
			break;
		case REPAIRS:
			int repairSum = payRepairs(communityChest.getHouseCost(),communityChest.getHotelCost(),player);
			if(player.pay(bank, repairSum))
				break;
			else 
				wentBankrupt = true;
			break;
		default:
			break;
			}
		
		return wentBankrupt;
	}
	
	/**Finds the Closest Value to the Target in an Array
	 * @param position
	 * @param numbers
	 * @return closest;
	 */
	public int closestTo(int position, int[] numbers) {
		int closest = -1;
		//If there is one before overlap
		for(int i = position+1; i<Board.BOARD_SIZE;i++) {
			closest = check(i, numbers);
			if(closest != -1)
				return closest;
		}
		//If there isn't one before the next go around the board
		for (int i = 0; i < Board.BOARD_SIZE; i++) {
			closest = check(i,numbers);
			if(closest != -1)
				return closest;
		}
		
		return closest;
	}

	/**Checks if the number is the closest to avoid repetitive code
	 * 
	 * @param num
	 * @param numbers
	 * @return
	 */
	public int check(int num, int[] numbers) {
		for(int i = 0; i < numbers.length; i++) {
			if(num == numbers[i])
				return numbers[i];
		}
		return -1;
	}
	
	/**Pay Repairs Card implementation method
	 * 
	 * @param houseCost
	 * @param hotelCost
	 * @param p
	 * @return
	 */
	public int payRepairs(int houseCost,int hotelCost,Player p) {
		ArrayList<PurchaseableSquare> squares = p.getHand();
		int numHouses = 0;
		int numHotels = 0;
	
		for(int i = 0; i<squares.size();i++) {
			if(squares.get(i) instanceof Property) {
				numHouses = numHouses + ((Property) squares.get(i)).getHouses();
				numHotels = numHotels + ((Property) squares.get(i)).getHotels();
			}
		}
		messenger.printHousesAndHotels(numHouses,numHotels);
		return numHouses*houseCost + numHotels*hotelCost;
	}
	
	/**Goes Through steps when a player wants to buy a square, to see if they can by it
	 * 
	 * @param player
	 * @param psq
	 * @param owned
	 */
	private void tryToBuy(Player player, PurchaseableSquare psq, boolean owned) {
		if(!owned) {
			if(checkBuy(player,psq)) {
				if(Buy(player,psq)) {
					messenger.bought(player, psq);
					psq.setOwner(player);
					player.addToHand(psq);
					}
				else {
					Bid finalBid = bank.auction(psq, players, debug);
					if(finalBid.getAmount()!=-1) {
						messenger.bought(finalBid.getBidder(), psq);
						psq.setOwner(finalBid.getBidder());
						finalBid.getBidder().addToHand(psq);
						finalBid.getBidder().pay(bank, finalBid.getAmount());
					}
				}
			}
			else {
				Bid finalBid = bank.auction(psq, players, debug);
				if(finalBid.getAmount()!=-1) {
					messenger.bought(finalBid.getBidder(), psq);
					psq.setOwner(finalBid.getBidder());
					finalBid.getBidder().addToHand(psq);
					finalBid.getBidder().pay(bank, finalBid.getAmount());
				}
			}
		}	
	}
	
	/**Checks whether a player wants to buy a square
	 * 
	 * @param player
	 * @param psq
	 * @return
	 */
	public boolean checkBuy(Player player, PurchaseableSquare psq) {
		messenger.printAvailableToBuy(psq, rentMultiplier);	
		return playerInput.getYesOrNo();
	}
	
	/**Checks whether a play can afford a square
	 * 
	 * @param player
	 * @param psq
	 * @return
	 */
	public boolean Buy(Player player, PurchaseableSquare psq) {
		int price = rentMultiplier*psq.getPrice();
		boolean canAfford = player.pay(bank, price);
		if(canAfford == false) {
			messenger.printNotEnoughMoney();
		}
		return canAfford;
	}

	/**Calculates the correct rent to be charged to the player who lands on a square owned by another player
	 * Works for Any type of purchaseable square
	 * 
	 * @param player
	 * @param psq
	 * @return
	 */
	public boolean chargeRent(Player player, PurchaseableSquare psq) {
		
		int rent = 0;
		Player owner = psq.getOwner();
		boolean wentBankrupt = false;
		
		if(psq.isMortaged()) {
			return wentBankrupt;
		}
		
		if(owner.equals(player)){
			messenger.printYouOwnThis();
			return wentBankrupt;
		}
		
		if(psq instanceof Property) {
			Property property = (Property) psq;
			ArrayList<Property> set = MonopolyBoard.getPropertySet(property.getColour());
			int numOwned = property.checkSetOwned(set);
			
			if(debug) {
				System.out.println("This is a property\nGame thinks you own: " + numOwned);
			}
			
			rent = property.getRent(numOwned);
			
		}
		
		else if(psq instanceof Utilities) {
			int numUtilitiesOwned = 1;
			Utilities utility = (Utilities) psq;
			//The Locations of all the Utilities on the Board
			ArrayList<Utilities> utilities = MonopolyBoard.getUtilitySet();
			//Since it must have an owner to get to this point, if the other one is also owned by the same person we know they own the set
			if(utilities.get(0).getOwner()!=null && utilities.get(0)!=null){
				if (utilities.get(0).getOwner().equals(utilities.get(1).getOwner())) {
					numUtilitiesOwned++;
				}
			}
			
			if(debug) {
				System.out.println("This is a utility\nGame thinks you own: " + numUtilitiesOwned);
			}

			rent = rentMultiplier*utility.getRent(numUtilitiesOwned, lastRoll);
		}
		
		else if(psq instanceof Transport) {
			int numTransportsOwned = 1;
			Transport transport = (Transport) psq;
			ArrayList<Transport > transports = MonopolyBoard.getTransportSet();
			for(int i = 0; i < transports.size(); i++) {
				if(transports.get(i).getOwner()!=null){
					if (transports.get(i).getOwner().equals(transport.getOwner()));
						numTransportsOwned++;
					}
				}
			
			if(debug) {
				System.out.println("This is a utility\nGame thinks you own: " + numTransportsOwned);
			}
			
			rent = rentMultiplier*transport.getRent(numTransportsOwned-1);
		}
		
		if(!player.pay(owner, rent))
			if(bankruptcyProcess(player, rent))
				wentBankrupt = true;
			else
				player.pay(owner, rent);
		
		return wentBankrupt;
	}
	
	/**Creates a list of winners of the game
	 * 
	 * @return list of winners
	 */
	public ArrayList<Player> tallyScore() {
		int highestScore = 0;
		ArrayList<Player> winners = new ArrayList<Player>();
		//Winner is the player with the highest score
		for(int i = 0; i < players.size(); i++) {
			if(players.get(i).sumPoints() > highestScore) {
				highestScore = players.get(i).sumPoints();
			}
		}
		//Account for draws
		for(int i = 0; i < players.size(); i++) {
			if(players.get(i).sumPoints() == highestScore)
				winners.add(players.get(i));
		}
		return winners;
	}
	
	/**
	 * 
	 * @param player
	 * @param amount
	 * @return
	 */
	public boolean bankruptcyProcess(Player player, int amount) {
		while(player.getMoney()<=amount) {
			messenger.enteringBankruptcy(player);
			int decision = playerInput.getNumber();
			switch(decision) {
				case 0:
					bank.sellHH(player, MonopolyBoard);
					break;
				case 1:
					mortgage(player);
					break;
				case 2:
					Trade(player, players);
					break;
				case 3:
					return true;
				default:
					messenger.printInvalidInput();
			}	
		}	
		
		return false;
	}

	//Getters and Setters for testing
	
	public ArrayList<Player> getPlayers() {
		return players;
	}

	public void setPlayers(ArrayList<Player> players) {
		this.players = players;
	}


	public void setChanceDeck(Deck chanceDeck) {
		this.chanceDeck = chanceDeck;
	}

	public Deck getCommunityChestDeck() {
		return communityChestDeck;
	}

	public void setCommunityChestDeck(Deck communityChestDeck) {
		this.communityChestDeck = communityChestDeck;
	}

	public void setDebug(boolean b) {
		debug = b;
	}
	

}
