package game;

import java.util.ArrayList;
import java.util.Scanner;
import board.PurchaseableSquare;

import player.Player;

/** Takes inputs from player and checks them, this version uses system.in
 * But if the game were made with a GUI this could be substituted in any way
 * By replacing this class and leaving the references the same
 */
public class Input {

	private Messenger messenger;
	private Scanner input;
	private boolean valid;
	private int index;
	private boolean keepGoing;

	public Input(){
		input = new Scanner(System.in);
		messenger = new Messenger();
		valid = false;
		keepGoing = true;
		index = 0;
	}

	


	/**Gets the number of players from input
	 * @param repeatFlag
	 * @return
	 */
	public int getNumPlayers(boolean repeatFlag) {
		int numberOfPlayers;
		messenger.startOfGame(repeatFlag);
    	numberOfPlayers = getNumber();
    	return numberOfPlayers;
	}

	/**Creates a list of players, getting their names from input
	 * @param tokens
	 * @param numberOfPlayers
	 * @param debug
	 * @return
	 */
	public ArrayList<Player> createPlayerList(ArrayList<String> tokens, int numberOfPlayers, boolean debug) {
		ArrayList<Player> players = new ArrayList<Player>();
		
		for(int i=1;i<=numberOfPlayers;i++){  
			
			String newName; 
	 		System.out.println("Enter a name for Player "+i); 
	 		newName = input.nextLine(); 
	 		
	 		/*" ", x and X are all not allowed names*/
	 		while(newName.equals(" ")||newName.equals("x")||newName.equals("X")||newName.equals("")) {
	    		System.out.println("Name not allowed");
	    		System.out.println("Please try again");    		
	    		newName=input.nextLine();  
	    	}
			System.out.println("Your name is now "+newName);
			
			int index=1;
			for(String s : tokens) {
			    System.out.println((index++)+": "+s);    		
			}
			
			System.out.print("Choose a Token for " + newName +" ");
	    	int newToken = getNumber();
	    	
	    	/*cannot choose a number outside the number of tokens*/
	    	while(newToken>=index||newToken<1) {
	    		System.out.println("Invalid Input, outside of range");
	    		System.out.println("Please try again");    		
	    		newToken=getNumber();
	    	}
	    	System.out.print("You chose " + tokens.get(newToken-1) +"\n");
	    	Player player = new Player(newName,tokens.get(newToken-1));
	    	tokens.remove(newToken-1);
	    	
	    	if(debug) {
	    		System.out.println("Player " + player.getName() +" was assigned " + player.getToken());
	    	}
	    	
	    	players.add(player);  	
     	} 
		return players;
	}


	/**Gets a yes or no response from the player
	 * @return
	 */
	public boolean getYesOrNo() {
		String decision;
		while(true) {
			decision = input.nextLine();
			switch(decision) {
			case "y":
				return true;
			case "Y":
				return true;
			case "n":
				return false;
			case "N":
				return false;
			default:
				messenger.printInvalidInput();
			}
		}
	}


	/**Gets an integer value from input
	 * @return
	 */
	public int getNumber() {	
		
		while (!input.hasNextInt()) 
	    {
	        input.nextLine(); //clear the invalid input
	        messenger.printInvalidInput();
	    }
	 
	    int num = input.nextInt();
	    input.nextLine();
	    return num;
	}
	/**Gets an integer value from input
	 * If input is X, returns
	 * @return
	 */
	public int getNumberOrExit() {	
			
			while (!input.hasNextInt()) 
		    {
		        input.nextLine(); //clear the invalid input
		        messenger.printInvalidInput();
		        messenger.printExit();
		        if(this.getYesOrNo()) {
		        	int num = -1;
		        	return num;
		        }
		    }
		 
		    int num = input.nextInt();
		    input.nextLine();
		    return num;
		}
	
	/**Gets a player name and compares it to the list of players
	 * @param players
	 * @param debug
	 * @return player name input
	 */
	public String getPlayerName(ArrayList<Player> players, boolean debug) {
		String playerName;
		playerName = input.nextLine();
		index = 0;
		valid = false;
		keepGoing = true;
		
		if(playerName.equals("x") || playerName.equals("X")){
			valid = true;
			keepGoing = false;
		}
		if(!valid) {
			for(index = 0; index < players.size(); index++) {
				if(playerName.equals(players.get(index).getName())) {
					valid = true;
					keepGoing = true;
					break;
				}		
			}
		}
		
		if(debug) {
			System.out.println("Index is: " + index);
		}

		return playerName;
	}
	
	/**Gets a player name except the player not wanted
	 * @param players
	 * @param debug
	 * @param notThisOne
	 * @return
	 */
	public String getPlayerNameExcept(ArrayList<Player> players, boolean debug, Player notThisOne) {
		String playerName;
		playerName = input.nextLine();
		index = 0;
		valid = false;
		keepGoing = true;
		
		if(playerName.equals("x") || playerName.equals("X")){
			keepGoing = false;
			valid = true;
		}
		

		if(!valid) {
			for(index = 0; index < players.size(); index++) {
				
				if(playerName.equals(players.get(index).getName())) {
					valid = true;
					keepGoing = true;
					break;
				}
				
			}
		}

		if(playerName.equals(notThisOne.getName())) {
			valid = false;
			keepGoing = true;
		}
		
		if(debug) {
			System.out.println("Index is: " + index);
		}

		return playerName;
	}
	
	/**Gets the name of the desired square
	 * @param hand
	 * @return name
	 */
	public String getSquareName(ArrayList<PurchaseableSquare> hand) {
		String propertyName;
		propertyName = input.nextLine();
		index = 0;
		valid = false;
		keepGoing = true;
		
		if(propertyName.equals("x") || propertyName.equals("X")){
			keepGoing = false;
			valid = true;
		}
		
		if(!valid) {
			for(index = 0; index < hand.size(); index++) {
				if(propertyName.equals(hand.get(index).getName())) {
					valid = true;
					keepGoing = true;
					break;
				}		
			}
		}

		return propertyName;
	}

	/**
	 * @return
	 */
	public Scanner getInput() {
		return input;
	}

	public void setInput(Scanner input) {
		this.input = input;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public void setKeepGoing(boolean keepGoing) {
		this.keepGoing = keepGoing;
	}

	public boolean isValid() {
		return valid;
	}
	
	public boolean isKeepGoing() {
		return keepGoing;
	}
	
	public int getIndex() {
		return index;
	}
}
	


