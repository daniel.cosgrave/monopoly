package game;

import java.util.ArrayList;
import board.Board;
import board.Property;
import board.PurchaseableSquare;
import player.Bank;
import player.People;
import player.Player;

/**Communicates with the player, this version uses system.out
 * as a way of communicating
 * This can be changed if you wish by creating a different version of messenger e.g with a GUI
 */
public class Messenger {

	
	public Messenger() {}
	
	/**Communicates the player's roll and if they got a double
	 * 
	 * @param roll1
	 * @param roll2
	 */
	public void printRollMessage(int roll1, int roll2) {
    	System.out.print("You rolled a "+roll1+" and a "+roll2+".\n");		
    	if(roll1==roll2) {
	    	System.out.print("You rolled a double!\n");		
    	}
    	
    	int roll3=roll1+roll2;
    	System.out.print("Total is "+roll3+"\n");		
	}
	
	public void printPlayerPosition(Player p, Board monopolyBoard) {
    	System.out.print(p.getName()+" is now on the "+getSquareName(p.getPosition(),monopolyBoard)+" square. ("+p.getPosition()+").\n");		

	}
	
	public void keepGoingPrompt() {
		System.out.println("\nWould any player like to take any additional actions?");
		System.out.println("Enter x at any time to move on");
	}
	
	/**All Possible Extra Moves
	 * 
	 */
	public void printKeepGoing() {
		System.out.println("What Actions?");
		System.out.println("0.Nothing Else");
		System.out.println("1.Buy houses or hotels");
		System.out.println("2.Sell houses or hotels");
		System.out.println("3.Mortgage/unmortgage");
		System.out.println("4.Trade");
		System.out.println("5.Check out the Board");
		System.out.println("6.Check Status");
	}
	
	public void printInvalidInput() {
		System.out.println("Invalid Input");
	}
	
	public void printExit() {
		System.out.println("Would you like to exit this transaction? (y/n)");
	}
	
	/**What options are presented to the player when they are in jail
	 * 
	 */
	public void printJailOptions() {
		System.out.println("You are in Jail. Please select an option:\n");	
		System.out.println("0.Wait and try to roll a double, max 3 tries before fine.\n");
    	System.out.println("1.Buy 'Get out of Jail Free' card from another player.\n");
    	System.out.println("2.Pay M50 and get out now.\n");
    	System.out.println("3.Use 'Get Out of Jail Free' card.");
	}
	
	public String getSquareName(int position, Board board) {
			return board.getBoard().get(position).getName();
		}
	
	/**Outputs the board including the locations of the players, ownership, houses,and hotels
	 * 
	 * @param board
	 * @param players
	 * @param bank
	 */
	public void printBoard(Board board ,ArrayList<Player> players, Bank bank) {
		
		for (int i = 0; i< Board.BOARD_SIZE;i++) {

			System.out.print(i + ": ");
			
			if(board.getBoard().get(i) instanceof Property) {
				System.out.print("(" + ((Property) board.getBoard().get(i)).getColour() + ") ");
			}
			
			System.out.print(board.getBoard().get(i).getName());
			
			if(board.getBoard().get(i) instanceof PurchaseableSquare) {
				if(((PurchaseableSquare) board.getBoard().get(i)).isOwned()) {
					System.out.print(" (Owned By " + ((PurchaseableSquare) board.getBoard().get(i)).getOwner().getName() + ")");
				}
			}
			
			if(board.getBoard().get(i) instanceof Property && ((Property) board.getBoard().get(i)).getHouses() != 0) {
				System.out.print("(" + ((Property) board.getBoard().get(i)).getHouses() + " Houses) ");
			}
			
			if(board.getBoard().get(i) instanceof Property && ((Property) board.getBoard().get(i)).getHotels() != 0) {
				System.out.print("(" + ((Property) board.getBoard().get(i)).getHotels() + " Hotels) ");
			}
			
			for(int j = 0;j < players.size();j++) {
				if(players.get(j).getPosition() == i)
					System.out.print(" (" + players.get(j).getToken() + ")");
			}
			
			System.out.print("\n");
		}
		System.out.print("\n");
		for(int j = 0;j < players.size();j++) {
			System.out.println(players.get(j).getName() + " Owns " + players.get(j).getHand().size() + " Properties");
		}
		System.out.println("Houses Left: " + bank.getRemainingHouses() + "\nHotels Left: " + bank.getRemainingHotels());
	}
	
	
	public void printNotEnoughMoney() {
		System.out.println("You don't have enough money for this action");
	}

	public void printCardAction(String details) {
		System.out.println("Drawing a card...");
		System.out.println(details);
	}

	public void printMoneyTransaction(People payer, int amount, People recipient) {
		if(amount >= 0)
			System.out.println("Transaction: " + payer.getName() + " has sent " + amount + " to " + recipient.getName());
		else
			System.out.println("Transaction: " + recipient.getName() + " has sent " + Math.abs(amount) + " to " + payer.getName());
	}

	public void printHousesAndHotels(int numHouses, int numHotels) {
		System.out.println("Paying for the repair of "+numHouses+" houses and "+numHotels+" hotels");
	}

	public void printAvailableToBuy(PurchaseableSquare psq, int rentMultiplier) {
		System.out.println("This property is available to buy for " + rentMultiplier*psq.getPrice() + "\nWould you like to buy it? (y/n)");
	}

	public void printIsMortgaged() {
		System.out.println("This property is mortgaged");
	}
	
	public void bought(Player p, PurchaseableSquare psq) {
		System.out.println("Congratulations " + p.getName() + " You now own " + psq.getName());
		System.out.println("You have M" + p.getMoney() + " left");
	}

	public void bought(Player p, String psq) {
		System.out.println("Congratulations " + p.getName() + " You now own " + psq);
		System.out.println("You have M" + p.getMoney() + " left");
	}
	
	public void turnHeader(Player player) {
		System.out.println("\n////////// It's " + player.getName() + "'s" + " Turn //////////");
		
	}

	public void printStatus(Player player,Board board) {
		System.out.println(player.getName() + " has M" + player.getMoney());
		printPlayerPosition(player,board);
		if(player.getHand().size() != 0) {
			System.out.println("And Owns These Squares:");
			printHand(player);
		}
		else
			System.out.println("And Owns No Properties");
		
		System.out.println("Current Score: " + player.sumPoints());
	}

	public void printPlayers(ArrayList<Player> players) {
		System.out.println("Select a player");
		for(int i = 0; i < players.size(); i++) {
			System.out.println(players.get(i).getName());
		}
	}

	public void printPlayersExcept(ArrayList<Player> players, Player notThisPlayer) {
		System.out.println("Select a player");
		for(int i = 0; i < players.size(); i++) {
			if(!players.get(i).equals(notThisPlayer))
			System.out.println(players.get(i).getName());
		}
	}
	
	public void printProperties(Player player) {
		if(player.getHand().size() != 0) {
			System.out.println("Properties " + player.getName() + " owns");
			for(int i = 0; i < player.getHand().size(); i++) {
				if(player.getHand().get(i) instanceof Property) {
					System.out.print("(" + ((Property) player.getHand().get(i)).getColour() + ") ");
					System.out.print(player.getHand().get(i).getName());
					System.out.print(" Houses: " + ((Property) player.getHand().get(i)).getHouses() + " ");
					System.out.print(" Hotels: " + ((Property) player.getHand().get(i)).getHotels() + " \n");
				}
			}
		}
		 
		else
			System.out.println(player.getName() + " Owns No Properties");
		
	}

	public void HotelPrompt() {
		System.out.println("This Property already has a hotel");
		
	}

	public void printTax(Player player) {
		System.out.println(player.getName()+" is charged a flat M200");
		
		
	}

	public void printAuction(ArrayList<Player> players, PurchaseableSquare psq) {
		System.out.println(psq.getName() + " Is Being Auctioned by the Bank");
		System.out.println("Who wants to bid? (type x for nobody)");
		
	}

	void printGetBid(String name, Bid currentBid) {
		System.out.println(name+ ", how much would you like to bid?");
		if(currentBid.getAmount() > 0)
			System.out.println("Bid an amount between 1 and "+currentBid.getAmount()+" to forfit.");
	}

	public void printNoValid() {
		System.out.println("No sets owned");
	}

	public void printTooManyHouses(int i) {
		if(i != 0)
			System.out.println("You have too many houses on this property, spread the love!");
		else
			System.out.println("You have too few houses on this property, spread the love!");
	}

	public void printBuilt() {
		System.out.println("You built a it!");
		
	}

	public void startOfGame(boolean repeatFlag) {
		if(!repeatFlag) {
			System.out.println("========== Welcome to a new game of Monopoly! ==========");
    		System.out.print("Enter a number of players (between 2 and 6):  \n");
		}
		else
			System.out.println("BETWEEN 2 AND 6");
	}

	public void printRemainingHouses(Bank bank) {
		System.out.println("Bank has " + bank.getRemainingHouses() + " Houses and " + bank.getRemainingHotels() + " Hotels");
	}

	public void printYouOwnThis() {
		System.out.println("This is your property");
		
	}

	public void GO() {
		System.out.println("You went past GO!");
		System.out.println("You are owed 200");
	}
	
	public void noGetOutOfJaillFreeCards() {
    	System.out.println("You do not have any Get Out Of Jail Free Cards");
	}

	public void noHousesOrHotels() {
		System.out.println("No Houses or Hotels on this property");
	}

	public void printSold() {
		System.out.println("You sold it!");
		
	}

	public void printHand(Player player) {
		for(int i = 0; i < player.getHand().size(); i++) {
			
			if(player.getHand().get(i) instanceof Property)
				System.out.print("(" + ((Property) player.getHand().get(i)).getColour() + ") ");
			
			System.out.print(player.getHand().get(i).getName());
			
			if(player.getHand().get(i) instanceof Property) 
				System.out.print(" Houses: " + ((Property) player.getHand().get(i)).getHouses() + " Hotels: " + ((Property) player.getHand().get(i)).getHotels());
			
			if(player.getHand().get(i).isMortaged())
				System.out.print(" (Mortgaged)");
				
			System.out.print("\n");
		}	
	}

	public void printEmptyHand() {
		System.out.println("Hand is empty.\n");
	}

	public void printMortgaged(PurchaseableSquare psq) {
		if(psq.isMortaged())
			System.out.println("Succesfully Mortgaged " + psq.getName());
		else
			System.out.println("Succesfully Un-Mortgaged " + psq.getName());
	}
	
	public void printTradeWith(Player p) {
		System.out.println("Who would like to trade with "+p.getName()+"?");
	}
	
	public void printTradeWhat() {
		System.out.println("\nWhat would you like to trade?");
		System.out.println("0. To Go Back.");
		System.out.println("1. Get out of jail free card.");
		System.out.println("2. A purchasable square.");
	}
	
	public void printGetTradeOffer(String name) {
		System.out.println("How much is "+name+" willing to offer?");
	}	
	
	public void printGetTradeProposition(String name) {
		System.out.println("How much is "+name+" willing to accept?");
	}	
	
	public void printTradeWhich() {
		System.out.println("Which purchasable square do you want to sell?");
	}

	public void printFinalScores(ArrayList<Player> players, ArrayList<Player> winners) {
		System.out.println("The game is over. Final Scores:");
		for(int i = 0; i <players.size(); i++) {
			System.out.println(players.get(i).getName() + ": " + players.get(i).sumPoints());
		}
		if(winners.size() == 1)
			System.out.println("Congratulations " + winners.get(0).getName() + "! You won!");
		else {
			System.out.println("It's a " + winners.size() + " way Draw!");
			System.out.println("Congratulations on the draw");
			for(int i = 0; i < winners.size(); i++) {
				System.out.println("     " + winners.get(i).getName());
			}
		}
	}
	
	public void printNoJailCard() {
		System.out.println("The person you are trying to buy from does not have any get out of jail free cards. ");	
	}
	
	public void printBoughtJailCard(Player buyer, Player seller, int amount) {
		System.out.println(buyer.getName()+" bought a Get Out Of Jail Free Card from "+seller.getName()+" for "+amount);	
	}

	void enteringBankruptcy(Player player) {
		System.out.println("This will bankrupt you, will you try and stay in the game?");
		System.out.println("0: Sell Houses or Hotels");
		System.out.println("1: Mortgage Squares");
		System.out.println("2: Trade with someone");
		System.out.println("3: Give up");
	}
	
}

