package player;

import java.util.ArrayList;
import java.util.Scanner;
import game.Input;
import board.Board;
import board.Property;
import board.PurchaseableSquare;
import game.Bid;
import game.Messenger;

public class Bank implements People {
	int remainingHouses;
	int remainingHotels;
	String name;
	Messenger messenger;
	Scanner input;
	Input playerInput;

	public Bank() {
		name = "The Bank";
		remainingHouses = 32;
		remainingHotels = 12;
		
		messenger = new Messenger();
		input = new Scanner(System.in);
		playerInput = new Input();
	}
	
    public String getName() {
    	return name;
    }
    
    public int getRemainingHouses() {
    	return remainingHouses;
    }
    
    public int getRemainingHotels() {
    	return remainingHotels;
    }

/**Buy Houses and Hotels
 * 
 * @param player
 * @return bought or not
 */
	public boolean buyHH(Player player, Board MonopolyBoard) {
		boolean isValid = false;
		boolean keepGoing = true;
		int i = 0;
		
		// Minimum number of properties to build anything is 2 so safe to quite if condition is met
		if(player.getHand().size() < 2 ) {
			messenger.printNoValid();
			return false;
		} 
		
		messenger.printProperties(player);
		messenger.printRemainingHouses(this);
		//Add press x to exit in messenger
		while(keepGoing) {
			playerInput.getSquareName(player.getHand());
			isValid = playerInput.isValid();
			keepGoing = playerInput.isKeepGoing();
			i = playerInput.getIndex();
			
			if(!isValid) {
				messenger.printInvalidInput();
			}
	
			else if(keepGoing) {
				Property tempProperty = (Property) player.getHand().get(i);
				
				if(!buildable(tempProperty, player, MonopolyBoard, i))
					return false;
					
				//Build A House
				if(tempProperty.getHouses() < 4) {
					if(remainingHouses > 0) {
						player.pay(this,tempProperty.getHousePrice());
						((Property) player.getHand().get(i)).buildHouse();
						remainingHouses--;
						messenger.printRemainingHouses(this);
						return true;
					}
					else {
						messenger.printRemainingHouses(this);
						return false;
					}
				}
				//Or if there are 4 houses build a hotel
				else {
					if(remainingHotels > 0) {
						player.pay(this,tempProperty.getHousePrice());
						((Property) player.getHand().get(i)).buildHotel();
						remainingHotels--;
						remainingHouses = remainingHouses+4;
						messenger.printRemainingHouses(this);
						return true;
					}
					else {
						messenger.printRemainingHouses(this);
						return false;
					}
				}	
			}
		}
	return false;	
	}

	public boolean sellHH(Player player, Board MonopolyBoard) {
		boolean isValid = false;
		boolean keepGoing = true;
		int i = 0;
		
		if(player.getHand().size() < 2 ) {
			messenger.printNoValid();
			return false;
		} 
		
		messenger.printProperties(player);
		//Add press x to exit in messenger
		while(keepGoing) {
			playerInput.getSquareName(player.getHand());
			isValid = playerInput.isValid();
			keepGoing = playerInput.isKeepGoing();
			i = playerInput.getIndex(); //The index in the player's hand where the property is
		
			if(isValid) {
				Property tempProperty = (Property) player.getHand().get(i); //Temp Property is here to make the code less messy looking
				int sellValue = tempProperty.getHousePrice()/2;
				
				if(tempProperty.getHotels()==0 && tempProperty.getHouses()==0) {
					messenger.noHousesOrHotels();
					return false;
				}
				
				ArrayList<Property> set = MonopolyBoard.getPropertySet(((Property) player.getHand().get(i)).getColour());
				
				if(maxDiffIfSold(set, (Property) player.getHand().get(i)) > 1) {
					messenger.printTooManyHouses(0);
					return false;
				}
				
				if(tempProperty.getHotels() == 1) {
					pay(player, sellValue);
					remainingHotels++;
					((Property) player.getHand().get(i)).sellHotel();
				}
				
				else {
					pay(player, sellValue);
					remainingHouses++;
					 ((Property) player.getHand().get(i)).sellHouse();
				}
				
				messenger.printRemainingHouses(this);	
				return true;
			}
			
			else
				messenger.printInvalidInput();
			
		}
		
		return false;
	}
	
	/**Contains all the tets for building a house/hotel
	 * 
	 * @param tempProperty
	 * @param player
	 * @param MonopolyBoard
	 * @param i
	 * @return
	 */
	private boolean buildable(Property tempProperty, Player player, Board MonopolyBoard, int i) {
		if(tempProperty.getHotels() > 0) {
			messenger.HotelPrompt();
			return false;
		}
		
		if(player.getMoney() <= tempProperty.getHousePrice()) {
			messenger.printNotEnoughMoney();
			return false;
		}
		
		ArrayList<Property> set = MonopolyBoard.getPropertySet(((Property) player.getHand().get(i)).getColour());
		int numOwned = ((Property) player.getHand().get(i)).checkSetOwned(set);
		
		if( !((Property) player.getHand().get(i)).checkWholeSetOwned(set, numOwned) ) {
			messenger.printNoValid();
			return false;
		}
		if(maxdiffs(set, (Property) player.getHand().get(i)) > 1) {
			messenger.printTooManyHouses(1);
			return false;
		}
		return true;
	}


	/**Max difference check for buying houses/hotels
	 * 
	 * @param set
	 * @param property
	 * @return
	 */
	public int maxdiffs(ArrayList<Property> set, Property property) {
		int numPotentialHouses = property.getHouses() + 1;
		int maxDifference = 0;
		int temp;
		
		if(property.getHouses() == 4 && property.getHotels() == 0)
			return 1;
		
		for(int i = 0; i<set.size(); i++) {
			if(!set.get(i).equals(property)) {
				temp = numPotentialHouses - set.get(i).getHouses();
				if(temp > maxDifference)
					maxDifference = temp;
			}
		}
		return maxDifference;
	}
	
	/**Max difference check for selling houses/hotels
	 * 
	 * @param set
	 * @param property
	 * @return
	 */
	public int maxDiffIfSold(ArrayList<Property> set, Property property){
		int numPotentialHouses = property.getHouses() - 1;
		int maxDifference = 0;
		int temp;
		
		if(property.getHouses() == 0 && property.getHotels() == 1)
			return 1;
		
		for(int i = 0; i<set.size(); i++) {
			if(!set.get(i).equals(property)) {
				temp = Math.abs(numPotentialHouses - set.get(i).getHouses());
				if(temp > maxDifference)
					maxDifference = temp;
			}
		}
		return maxDifference;
	}
	
	//------------------------------------------------------
	
	/**Hosts auctions when a property is not purchased
	 * 
	 * @param psq
	 * @param players
	 * @param debug
	 * @return
	 */
	public Bid auction(PurchaseableSquare psq, ArrayList<Player> players, boolean debug) {
	
		Bid currentBid= new Bid();
		Bid newBid = new Bid();
	
		
		boolean keepGoing=true;
		
		while (keepGoing) {

			keepGoing = newBid.getBid(psq,players, debug, currentBid);
			
			if(keepGoing) {
				if (newBid.getAmount()>currentBid.getAmount()) {
					currentBid.setBid(newBid.getAmount(), newBid.getBidder());
				}
				
				else if(newBid.getAmount()<=0) {
					messenger.printInvalidInput();
				}
				
				else if(newBid.getAmount()==currentBid.getAmount()) {
					messenger.printInvalidInput();
				}
				
				else keepGoing = false; 		//for now, getBid() MUST return Some value, maybe 0 if they give up? internally
				
			}
			
			else
				currentBid.setBid(-1, null);
		}
		return currentBid;
	}
	
	/**Simple way to send money to a player from the bank
	 * 
	 * @param Recipient
	 * @param amount
	 */
	public void pay(Player Recipient, int amount) {
		messenger.printMoneyTransaction(this, amount, Recipient);
		Recipient.setMoney(Recipient.getMoney()+amount); 
	}

	public Scanner getInput() {
		return input;
	}

	public void setPlayerInput(Input playerInput) {
		this.playerInput = playerInput;
	}

	public void setRemainingHouses(int remainingHouses) {
		this.remainingHouses = remainingHouses;
	}

	public void setRemainingHotels(int remainingHotels) {
		this.remainingHotels = remainingHotels;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	//------------------------------------------------------	
	}