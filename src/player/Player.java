package player;

import java.util.ArrayList;

import board.*;
import game.Messenger;

/**Player Class
 * 
 *
 */
public class Player implements People {
	String name;
	private String token;
	private int money;
	private ArrayList<PurchaseableSquare> hand;
	private int doubleCount;
	private int position;
	private boolean isInJail;
	int jailFreeCards;
	int escapingJail;
	Messenger messenger;
	
	//-----------Set Up--------------------
	
	//constructor that sets Name and Token
	public Player(String newName, String newToken) {
		this.name = newName;
		this.token = newToken;
		doubleCount=0;
		position=0;	//starts at the beginning
		money=1500;	
		isInJail = false;
		jailFreeCards=0;
		escapingJail=0;
		hand =  new ArrayList<PurchaseableSquare>();
		messenger = new Messenger();
	}
	

	
	//--------Monetary Transactions--------
	
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}

	public void receiveMoney(int money) {
		this.money=getMoney() + money;		
	}
	
	/**
	 * Returns true if Player was has enough money to complete 
	 * transaction and money is successfully paid.
	 * Will return false otherwise.
	 * @param Recipient
	 * @param amount
	 * @return
	 */
	public boolean pay(People Recipient, int amount) {
		Messenger messenger = new Messenger();
		
		//PAYING TO PLAYER
		if (Recipient instanceof Player) {
			Player payer = (Player) Recipient;
			if(this.money>=amount) {
				messenger.printMoneyTransaction(this, amount, Recipient);
				this.money=getMoney() - amount; 
				payer.receiveMoney(amount);
				return true;
			}
			else return false;
		}
		
		//PAYING TO BANK
		if (Recipient instanceof Bank) {
			if(this.money>=amount) {
				messenger.printMoneyTransaction(this, amount, Recipient);
				this.money=getMoney() - amount; 
				return true;
			}
			
			else return false;
				
		}
		
		else return false;
	}
	
	//---------Hand Functions--------------- 
	
	public ArrayList<PurchaseableSquare> getHand() {
		return hand;
	}

	public void setHand(ArrayList<PurchaseableSquare> hand) {
		this.hand = hand;
	}
	
	public void addToHand(PurchaseableSquare newSquare) {
		getHand().add(newSquare);
	}
	
	public void removeFromHand(Square square) {
		getHand().remove(square);
	}
	//---------Moving-----------------------
	
	public int getPosition() {
		return position;
	}
	
	public void movePlayer(int spaces) {
		this.position += spaces;
	}
	/**Rolls two dice
	 * 
	 * @return two dice roll int array
	 */
	public int[] twoDiceRoll() {
		Messenger messenger = new Messenger();
		SixSidedDie die = new SixSidedDie();
		int roll1 = die.getRoll();
		int roll2 = die.getRoll();	
		messenger.printRollMessage(roll1, roll2);
		int[] roll= {roll1,roll2};
		return roll;
	}
	
	/**Moves a player by the amount in two rolls and records their doubles
	 * 
	 * @param roll
	 */
	public void movePlayer(int[]roll) { 
				
		int newPosition=roll[0]+roll[1]+position;
		
		if (roll[0]==roll[1]) doubleCount++;
		else doubleCount = 0;
		
		if(newPosition>=40) {
			Bank bank = new Bank();
			messenger.GO();
			bank.pay(this, 200);
		}

		//check if 3rd 
		if(doubleCount==3) {
			this.GoToJail();
			doubleCount = 0;
		}
		else {
			position= newPosition%40; //%40 to deal with roll over of board. ie. board only has 40 
		}
				
	}

	/**
	 * Moves player instantly to anywhere except jail.
	 * @param newPosition
	 */
	public void goTo(int newPosition) {
		
		//A card will never send you to itself
		if (this.position>newPosition) {
			Bank bank = new Bank();
			messenger.GO();
			bank.pay(this, 200);
		}
			this.position=newPosition;
		
	}
//---------------------Jail-------------------------
	public void GoToJail() {
		this.position = 10;
		isInJail = true;
	}

	public boolean isInJail() {
		return isInJail;
	}

	public int getJailFreeCards(){
		return jailFreeCards;
	}
	
	public void aquiredJailFreeCard(){
		jailFreeCards++;
	}
	
	public void getOutOfJail() {
    	System.out.print("You got out of jail\n");
    	isInJail=false;
	}
	
	public void setEscapingJail(int set){
		escapingJail=set;
	}
	
	public int getEscapingJail() {
		return escapingJail;
	}
	
	public boolean useJailFreeCard() {
		if (jailFreeCards==0) {
			messenger.noGetOutOfJaillFreeCards();
			return false;
		}
		else{
			jailFreeCards--;
			return true;
		}
	}

//-------------------Trading-------------------------

	/**
	 * sellProperty returns true if the property was sold successfully. 
	 * Returns false if the buyer has insufficient funds or if the property still has hotels or houses.
	 * @param Buyer
	 * @param p
	 * @param money
	 * @return
	 */
	//is this too much for one method? Maybe check development somewhere else or add different exit flags?
	public boolean sellProperty(Player Buyer, PurchaseableSquare p, int money) {
		
		if(p instanceof Property) {
			Property P = (Property) p;

				if((P.getHotels()!=0)||(P.getHouses()!=0)) return false; //if property has no development
				
				else {
					if(Buyer.pay(this, money)) { //pay() returns true if Buyer is able to pay only
						this.removeFromHand(P);
						Buyer.addToHand(P);
						return true;
					}
					else return false;
				}
		}
	
		if(p instanceof Utilities) {
			Utilities U = (Utilities) p;
			
			if(Buyer.pay(this, money)) {
				this.removeFromHand(U);
				Buyer.addToHand(U);
				return true;
			}
			else return false;
		}

		if(p instanceof Transport) {
			Transport T = (Transport) p;
			
			if(Buyer.pay(this, money)) {
				this.removeFromHand(T);
				Buyer.addToHand(T);
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	

	/**Pays everyone a certain amount each
	 * 
	 * @param players
	 * @param amountEach
	 * @param bank
	 * @return
	 */
	public boolean payAll(ArrayList<Player> players, int amountEach) {
		int total = amountEach * (players.size()-1);
		boolean canAfford = false;
		if(canAfford = this.money >= total) {
			for (int i = 0; i < players.size();i++) {
				if(!players.get(i).equals(this))
					this.pay(players.get(i), amountEach);
			}
		}
		return canAfford;	
	}
	
	/**Sums all the scores for the win condition, cash, property value and house/hotel value
	 * 
	 * @return sum points
	 */
	public int sumPoints() {
		int handScore = 0;
		int squareScore = 0;
		int houseScore = 0;
		int hotelScore = 0;
		for(int i = 0; i < hand.size(); i++) {
			if(!hand.get(i).isMortaged()) {
				squareScore = hand.get(i).getPrice();
				if(hand.get(i) instanceof Property) {
					houseScore = ((Property) hand.get(i)).getHouses()*((Property) hand.get(i)).getHousePrice();
					hotelScore = ((Property) hand.get(i)).getHouses()*((Property) hand.get(i)).getHousePrice()*4;
				}
				handScore = handScore + houseScore + hotelScore + squareScore;
			}
		}
		
		return handScore + money;
	}

	public Messenger getMessenger() {
		return messenger;
	}

	public void setMessenger(Messenger messenger) {
		this.messenger = messenger;
	}

	public void setDoubleCount(int doubleCount) {
		this.doubleCount = doubleCount;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setInJail(boolean isInJail) {
		this.isInJail = isInJail;
	}

	public void setJailFreeCards(int jailFreeCards) {
		this.jailFreeCards = jailFreeCards;
	}
	
	public void setName(String newName) {
		this.name = newName;
	}
	
	public void setToken(String newToken) {
		this.token = newToken;
	}
	
	public String getToken() {
		return token;
	}
	
	public String getName() {
		return name;
	}
	
	public int getDoubleCount() {
		return doubleCount;
	}
	
}