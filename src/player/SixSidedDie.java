package player;

import java.util.Random;

/**A six sided die
 *
 */
public class SixSidedDie implements Dice {

	/**A six sided die roll
	 * 
	 */
	public int getRoll() {
		Random rand = new Random();
		int roll;
		
		roll = rand.nextInt(6) + 1;    
	    return roll;
	}
}



