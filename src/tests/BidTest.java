package tests;
import static org.junit.Assert.*;
import org.junit.*;
import player.*;
import game.*;

public class BidTest {
	
	@Test
	public void testSetBid() {
		Player player1=new Player("name1", "token1");
		Bid bid =new Bid();
		bid.setBid(100, player1);
		
		assertEquals(100,bid.getAmount());
	}
	
	
	
}
