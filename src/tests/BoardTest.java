package tests;
import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.*;
import board.*;
import player.Bank;
import player.Player;

public class BoardTest {
	
	@Test
	public void testConstruct() {
		Board board = new Board();
		assertEquals("Board Does Not Construct", 40, board.getBoard().size());
	}
	
	public void testPropSet() {
		Board board = new Board();
		ArrayList<Property> testSet = board.getPropertySet(Colour.BROWN);
		ArrayList<Property> validationSet = new ArrayList<Property>();
		Property prop1 = new Property("MAVEN HEAD", 1, Colour.BROWN,60,50,2,10,30,90,160,250,30);
		Property prop2 = new Property("CORK CITY GAOIL",2, Colour.BROWN, 60,50,4,20,60,180,320,450,30);
	
		testSet = board.getPropertySet(Colour.BROWN);
		
		validationSet.add(prop1);
		validationSet.add(prop2);
		
		assertEquals("Sets not creating properly", testSet.equals(validationSet));
	}
	
	@Test
	public void testUtilSet() {
		Board board = new Board();
		ArrayList<Utilities> testSet = board.getUtilitySet();
		ArrayList<Utilities> validationSet = new ArrayList<Utilities>();
		Utilities util1 = new Utilities("ELECTRIC COMPANY", 12, 150, 75);
		Utilities util2 = new Utilities("THE LIFETIME LAB", 26, 150, 75);	
		validationSet.add(util1);
		validationSet.add(util2);
		assertTrue("Sets not creating properly", testSet.get(0).getName().equals(util1.getName()));
		assertTrue("Sets not creating properly", testSet.get(1).getName().equals(util2.getName()));
	}
		
	@Test
	public void testTranSet() {
		Board board = new Board();
		ArrayList<Transport> testSet = board.getTransportSet();
		ArrayList<Transport> validationSet = new ArrayList<Transport>();
		Transport tran1 = new Transport("KENT STATION", 5, 200, 100);
		Transport tran2 = new Transport("BUS STATION", 15, 200, 100);
		Transport tran3 = new Transport("CORK AIRPORT", 25, 200, 100);
		Transport tran4 = new Transport("PORT OF CORK", 35, 200, 100);
		
		validationSet.add(tran1);
		validationSet.add(tran2);
		validationSet.add(tran3);
		validationSet.add(tran4);
		
		assertTrue("1", testSet.get(0).getName().equals(tran1.getName()));
		assertTrue("2", testSet.get(1).getName().equals(tran2.getName()));
		assertTrue("3", testSet.get(2).getName().equals(tran3.getName()));
		assertTrue("4", testSet.get(3).getName().equals(tran4.getName()));
	}
	
	@Test
	public void testOwnedSet() {
		Property prop1 = new Property("MIZHEN HEAD", 1, Colour.BROWN,60,50,2,10,30,90,160,250,30);
		Property prop2 = new Property("CORK CITY GAOIL",2, Colour.BROWN, 60,50,4,20,60,180,320,450,30);
		ArrayList<Property> validationSet = new ArrayList<Property>();
		validationSet.add(prop1);
		validationSet.add(prop2);
		Player player = new Player("Name", "Token");
		prop1.setOwner(player);
		assertFalse("Property owned check not working", prop1.checkWholeSetOwned(validationSet, prop1.checkSetOwned(validationSet)));
		prop2.setOwner(player);
		assertTrue("Property owned check not working", prop1.checkWholeSetOwned(validationSet, prop1.checkSetOwned(validationSet)));
	}
	@Test
	public void testBuyHouses() {
		Bank bank = new Bank();
		bank.setRemainingHouses(1);
		bank.setRemainingHotels(1);
		
		Player player = new Player("Name", "Token");
		Board board = new Board();
		
		((Property) board.getBoard().get(1)).setOwner(player);
		((Property) board.getBoard().get(3)).setOwner(player);
		
		player.addToHand((PurchaseableSquare) board.getBoard().get(1));
		
		//Buying a house when you don't own the whole set
		assertFalse("Should have not been able to buy house",bank.buyHH(player, board));
		assertEquals("Houses did not change", 0, ((Property) player.getHand().get(0)).getHouses());
		
		player.addToHand((PurchaseableSquare) board.getBoard().get(3));
		((Property) player.getHand().get(0)).setHouses(1);
		
		//Buying a house when the number is unequal
		System.out.println("Expected Input: \"CORK CITY GAOL\"");
		assertFalse("Should NOT have been able to buy house",bank.buyHH(player, board));
		assertEquals("Houses changed", 1, ((Property) player.getHand().get(0)).getHouses());
		
		//Buying a house correctly
		System.out.println("Expected Input: \"MIZHEN HEAD\"");
		assertTrue("Should have been able to buy house",bank.buyHH(player, board));
		assertEquals("Houses did not change", 1, ((Property) player.getHand().get(0)).getHouses());
		
		((Property) player.getHand().get(0)).setHouses(4);
		((Property) player.getHand().get(1)).setHouses(4);

		//Buying a hotel correctly
		System.out.println("Expected Input: \"MIZHEN HEAD\"");
		assertTrue("Should have been able to buy hotel",bank.buyHH(player, board));
		assertEquals("Houses did not change", 0, ((Property) player.getHand().get(1)).getHouses());
		assertEquals("Hotels did not change", 1, ((Property) player.getHand().get(1)).getHotels());
		assertEquals("Houses did not change for bank",4,bank.getRemainingHouses());
		assertEquals("Houses did not change for bank",0,bank.getRemainingHotels());
		
		//No hotels left
		System.out.println("Expected Input: \"MIZHEN HEAD\"");
		assertFalse("Should not have been able to buy Hotel",bank.buyHH(player, board));
		assertEquals("Houses did not change", 0, ((Property) player.getHand().get(1)).getHouses());
		assertEquals("Hotels did not change", 1, ((Property) player.getHand().get(1)).getHotels());
		assertEquals("Houses did not change for bank",4,bank.getRemainingHouses());
		assertEquals("Houses did not change for bank",0,bank.getRemainingHotels());
		
		//Buying a hotel when there are no hotels left
		System.out.println("Expected Input: \"CORK CITY GAOL\"");
		assertFalse("Should not have been able to buy Hotel",bank.buyHH(player, board));
		assertEquals("Houses did not change", 4, ((Property) player.getHand().get(0)).getHouses());
		assertEquals("Hotels did not change", 0, ((Property) player.getHand().get(0)).getHotels());
		assertEquals("Houses did not change for bank",4,bank.getRemainingHouses());
		assertEquals("Houses did not change for bank",0,bank.getRemainingHotels());
	}
	
	@Test
	public void testSellHouses() {
		
		Bank bank = new Bank();
		bank.setRemainingHouses(0);
		bank.setRemainingHotels(0);
		
		Player player = new Player("Name", "Token");
		Board board = new Board();
		
		((Property) board.getBoard().get(1)).setOwner(player);
		((Property) board.getBoard().get(3)).setOwner(player);
		
		player.addToHand((PurchaseableSquare) board.getBoard().get(1));
		player.addToHand((PurchaseableSquare) board.getBoard().get(3));
		
		System.out.println("Expected Input: \"MIZHEN HEAD\"");
		assertFalse("Can sell a house when there are no houses", bank.sellHH(player, board));
		assertEquals("Number of houses should be 0", 0, ((Property) player.getHand().get(1)).getHouses());
		
		((Property) player.getHand().get(1)).setHouses(1);
		System.out.println("Expected Input: \"MIZHEN HEAD\"");
		assertTrue("Can't sell a house when should be able to", bank.sellHH(player, board));
		assertEquals("Number of houses should be 0", 0, ((Property) player.getHand().get(1)).getHouses());
		
		((Property) player.getHand().get(1)).setHotels(1);
		System.out.println("Expected Input: \"MIZHEN HEAD\"");
		assertTrue("Can't sell a house when should be able to", bank.sellHH(player, board));
		assertEquals("Number of houses should be 4", 4, ((Property) player.getHand().get(1)).getHouses());
		assertEquals("Number of hotels should be 0", 0, ((Property) player.getHand().get(1)).getHotels());
		
		((Property) player.getHand().get(1)).setHouses(3);
		((Property) player.getHand().get(0)).setHouses(4);
		
		System.out.println("Expected Input: \"MIZHEN HEAD\"");
		assertFalse("Can sell houses even though max diff should prevent", bank.sellHH(player, board));
		assertEquals("Number of houses should be 3", 3, ((Property) player.getHand().get(1)).getHouses());
		assertEquals("Number of hotels should be 0", 0, ((Property) player.getHand().get(1)).getHotels());
	}
	
	
	
}