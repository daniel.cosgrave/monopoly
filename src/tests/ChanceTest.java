package tests;
import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import org.junit.*;

import board.Board;
import board.Property;
import board.PurchaseableSquare;
import game.*;
import player.*;
import cards.*;

public class ChanceTest {
	
	@Test //Get Out of Jail Free
	public void card1Test() {
		ByteArrayInputStream in = new ByteArrayInputStream("y".getBytes());
		System.setIn(in);
		
		Game game = new Game();
		Deck chanceDeck = new Deck(CardType.CHANCE);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i = 0; i<16; i++)
			arr.add(1);
		//Guarantee the first card
		arr.set(0, 0);
		chanceDeck.setDeck(arr);
		game.setChanceDeck(chanceDeck);
		Player player1 = new Player("Name1","Token1");
		Player player2 = new Player("Name2","Token2");
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		game.setPlayers(players);
		
		game.chanceInteractions(players.get(0));
		assertEquals(1,players.get(0).getJailFreeCards());
	}
	
	@Test //Go to a square
	public void card2Test() {
		Game game = new Game();
		Deck chanceDeck = new Deck(CardType.CHANCE);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i = 0; i<16; i++)
			arr.add(1);
		//Guarantee the first card
		arr.set(0, 1);
		chanceDeck.setDeck(arr);
		game.setChanceDeck(chanceDeck);	
		Player player1 = new Player("Name1","Token1");
		Player player2 = new Player("Name2","Token2");
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		game.setPlayers(players);
		
		ByteArrayInputStream in = new ByteArrayInputStream(("y").getBytes());
		System.setIn(in);
		
		game.chanceInteractions(game.getPlayers().get(0));
		assertEquals(11,game.getPlayers().get(0).getPosition());
	}
	
	@Test //Go to nearest Square
	public void card3Test() {
		Game game = new Game();
		Deck chanceDeck = new Deck(CardType.CHANCE);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i = 0; i<16; i++)
			arr.add(1);
		//Guarantee the first card
		arr.set(0, 2);
		chanceDeck.setDeck(arr);
		game.setChanceDeck(chanceDeck);	
		Player player1 = new Player("Name1","Token1");
		Player player2 = new Player("Name2","Token2");
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		game.setPlayers(players);
		
		game.chanceInteractions(game.getPlayers().get(0));
		assertEquals(5,game.getPlayers().get(0).getPosition());
		assertEquals(1100,game.getPlayers().get(0).getMoney());
	}
	
	@Test //Sends you money
	public void card4Test() {
		Game game = new Game();
		Deck chanceDeck = new Deck(CardType.CHANCE);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i = 0; i<16; i++)
			arr.add(1);
		//Guarantee the first card
		arr.set(0, 3);
		chanceDeck.setDeck(arr);
		game.setChanceDeck(chanceDeck);	
		Player player1 = new Player("Name1","Token1");
		Player player2 = new Player("Name2","Token2");
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		game.setPlayers(players);
		
		game.chanceInteractions(game.getPlayers().get(0));
		assertEquals(0,game.getPlayers().get(0).getPosition());
		assertEquals(1650,game.getPlayers().get(0).getMoney());
	}
	
	@Test //Go to a square and pass go
	public void card5Test() {
		ByteArrayInputStream in = new ByteArrayInputStream("y".getBytes());
		System.setIn(in);
		Game game = new Game();
		Deck chanceDeck = new Deck(CardType.CHANCE);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i = 0; i<16; i++)
			arr.add(1);
		//Guarantee the first card
		arr.set(0, 4);
		chanceDeck.setDeck(arr);
		game.setChanceDeck(chanceDeck);	
		Player player1 = new Player("Name1","Token1");
		Player player2 = new Player("Name2","Token2");
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		game.setPlayers(players);
		game.getPlayers().get(0).setPosition(30);
		
		game.chanceInteractions(game.getPlayers().get(0));
		assertEquals(24,game.getPlayers().get(0).getPosition());
		assertEquals(1460,game.getPlayers().get(0).getMoney());
	}
	
	@Test //Pay Everyone
	public void card6Test() {
		Game game = new Game();
		Deck chanceDeck = new Deck(CardType.CHANCE);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i = 0; i<16; i++)
			arr.add(1);
		//Guarantee the first card
		arr.set(0, 6);
		chanceDeck.setDeck(arr);
		game.setChanceDeck(chanceDeck);	
		Player player1 = new Player("Name1","Token1");
		Player player2 = new Player("Name2","Token2");
		Player player3 = new Player("Name3","Token3");
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		players.add(player3);
		game.setPlayers(players);
		
		game.chanceInteractions(game.getPlayers().get(0));
		assertEquals(0,game.getPlayers().get(0).getPosition());
		assertEquals(1400,game.getPlayers().get(0).getMoney());
	}
	
	@Test //Go back 3
	public void card7Test() {
		ByteArrayInputStream in = new ByteArrayInputStream("y".getBytes());
		System.setIn(in);
		Game game = new Game();
		Deck chanceDeck = new Deck(CardType.CHANCE);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i = 0; i<16; i++)
			arr.add(1);
		//Guarantee the first card
		arr.set(0, 7);
		chanceDeck.setDeck(arr);
		game.setChanceDeck(chanceDeck);	
		Player player1 = new Player("Name1","Token1");
		Player player2 = new Player("Name2","Token2");
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		game.setPlayers(players);
		game.getPlayers().get(0).setPosition(13);
		
		game.chanceInteractions(game.getPlayers().get(0));
		assertEquals(10,game.getPlayers().get(0).getPosition());
		assertEquals(1500,game.getPlayers().get(0).getMoney());
	}
	
	@Test //Repairs
	public void card8Test() {
		ByteArrayInputStream in = new ByteArrayInputStream("y".getBytes());
		System.setIn(in);
		Game game = new Game();
		Board board = new Board();
		Deck chanceDeck = new Deck(CardType.CHANCE);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i = 0; i<16; i++)
			arr.add(1);
		//Guarantee the first card
		arr.set(0, 8);
		chanceDeck.setDeck(arr);
		game.setChanceDeck(chanceDeck);	
		Player player1 = new Player("Name1","Token1");
		player1.addToHand((PurchaseableSquare) board.getBoard().get(1));
		player1.addToHand((PurchaseableSquare) board.getBoard().get(3));
		
		((Property) player1.getHand().get(0)).setHotels(1);
		((Property) player1.getHand().get(1)).setHouses(1);
		
		Player player2 = new Player("Name2","Token2");
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		game.setPlayers(players);
	
		
		game.chanceInteractions(game.getPlayers().get(0));
		assertEquals(0,game.getPlayers().get(0).getPosition());
		assertEquals(1375,game.getPlayers().get(0).getMoney());
	}
	
}