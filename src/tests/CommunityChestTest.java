package tests;
import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import org.junit.*;

import board.Board;
import board.Property;
import board.PurchaseableSquare;
import game.*;
import player.*;
import cards.*;

public class CommunityChestTest {
	
	@Test //Get Out of Jail Free
	public void card1Test() {
		ByteArrayInputStream in = new ByteArrayInputStream("y".getBytes());
		System.setIn(in);
		
		Game game = new Game();
		Deck communityChestDeck = new Deck(CardType.COMMUNITY);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i = 0; i<16; i++)
			arr.add(1);
		//Guarantee the first card
		arr.set(0, 0);
		communityChestDeck.setDeck(arr);
		game.setCommunityChestDeck(communityChestDeck);
		Player player1 = new Player("Name1","Token1");
		Player player2 = new Player("Name2","Token2");
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		game.setPlayers(players);
		
		game.communityChestInteractions(players.get(0));
		assertEquals(1,players.get(0).getJailFreeCards());
	}
	
	@Test //Get Money
	public void card2Test() {
		Game game = new Game();
		Deck communityChestDeck = new Deck(CardType.COMMUNITY);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i = 0; i<16; i++)
			arr.add(1);
		//Guarantee the first card
		arr.set(0, 1);
		communityChestDeck.setDeck(arr);
		game.setCommunityChestDeck(communityChestDeck);
		Player player1 = new Player("Name1","Token1");
		Player player2 = new Player("Name2","Token2");
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		game.setPlayers(players);
		
		game.communityChestInteractions(players.get(0));
		assertEquals(1550,players.get(0).getMoney());
	}
	
	@Test //Get Money
	public void card3Test() {
		Game game = new Game();
		Deck communityChestDeck = new Deck(CardType.COMMUNITY);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i = 0; i<16; i++)
			arr.add(1);
		//Guarantee the first card
		arr.set(0, 2);
		communityChestDeck.setDeck(arr);
		game.setCommunityChestDeck(communityChestDeck);
		Player player1 = new Player("Name1","Token1");
		Player player2 = new Player("Name2","Token2");
		Player player3 = new Player("Name3","Token3");
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		players.add(player3);
		game.setPlayers(players);
		
		game.communityChestInteractions(players.get(0));
		assertEquals(1520,players.get(0).getMoney());
	}
	
	@Test //Get Money
	public void card4Test() {
		
		Game game = new Game();
		Deck communityChestDeck = new Deck(CardType.COMMUNITY);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i = 0; i<16; i++)
			arr.add(1);
		//Guarantee the first card
		arr.set(0, 3);
		communityChestDeck.setDeck(arr);
		game.setCommunityChestDeck(communityChestDeck);
		Player player1 = new Player("Name1","Token1");
		Player player2 = new Player("Name2","Token2");
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		game.setPlayers(players);
		
		game.communityChestInteractions(players.get(0));
		assertEquals(1600,players.get(0).getMoney());
	}
	
	@Test //Get Money
	public void card5Test() {
		Board board = new Board();
		Game game = new Game();
		Deck communityChestDeck = new Deck(CardType.COMMUNITY);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i = 0; i<16; i++)
			arr.add(1);
		//Guarantee the first card
		arr.set(0, 7);
		communityChestDeck.setDeck(arr);
		game.setCommunityChestDeck(communityChestDeck);
		Player player1 = new Player("Name1","Token1");
		player1.addToHand((PurchaseableSquare) board.getBoard().get(1));
		player1.addToHand((PurchaseableSquare) board.getBoard().get(3));
		
		((Property) player1.getHand().get(0)).setHotels(1);
		((Property) player1.getHand().get(1)).setHouses(1);
		
		Player player2 = new Player("Name2","Token2");
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		game.setPlayers(players);
		
		game.communityChestInteractions(players.get(0));
		assertEquals((1500-115-40),players.get(0).getMoney());
	}
	
	
}