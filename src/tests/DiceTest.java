package tests;
import static org.junit.Assert.*;
import player.*;

public class DiceTest {
	
	public void testGetRoll() {
		SixSidedDie ssd = new SixSidedDie();
		
		int roll = ssd.getRoll();
		
		assertNotEquals("Roll was greater than 0",0,roll);
	}
}
