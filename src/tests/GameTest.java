package tests;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;

import org.junit.*;

import board.Board;
import board.Colour;
import board.Property;
import board.PurchaseableSquare;
import game.*;
import player.*;
public class GameTest {

	Game game = new Game();
	
	@Test
	public void testTallyScore() {
		
		String testString="token";
		Player player1 = new Player("name1",testString);
		Player player2 = new Player("name2",testString);
		Player player3 = new Player("name3",testString);
		Player player4 = new Player("name4",testString);
		
		player1.setMoney(2000);
		player2.setMoney(0);
		player3.setMoney(300);
		player4.setMoney(0);
		
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);	
		players.add(player3);
		players.add(player4);
		
		game.setPlayers(players);
		
		ArrayList<Player> winners=new ArrayList<Player>();
		winners.add(player1);
		winners.add(player3);
		
		ArrayList<Player> results = game.tallyScore();
		
		System.out.println("Should be "+winners.get(0).getName());
		System.out.println("Getting "+results.get(0).getName());

		
		assertEquals("Tallyed correctly",winners.get(0).getName(),results.get(0).getName());
	
	}
	
	@Test
	public void testDrawWinners() {
		
		String testString="token";
		Player player1 = new Player("name1",testString);
		Player player2 = new Player("name2",testString);
		Player player3 = new Player("name3",testString);
		Player player4 = new Player("name4",testString);
		
		player1.setMoney(2000);
		player2.setMoney(0);
		player3.setMoney(2000);
		player4.setMoney(0);
		
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);	
		players.add(player3);
		players.add(player4);
		
		game.setPlayers(players);
		
		ArrayList<Player> winners=new ArrayList<Player>();
		winners.add(player1);
		winners.add(player3);
		
		ArrayList<Player> results = game.tallyScore();
		
		System.out.println("Should be "+winners.get(0).getName()+" and "+winners.get(1).getName());
		System.out.println("Getting "+results.get(0).getName()+" and "+results.get(1).getName());

		
		assertEquals("Tallyed correctly",winners.get(0).getName(),results.get(0).getName());
		assertEquals("Tallyed correctly",winners.get(1).getName(),results.get(1).getName());
	
	}
	
	public void testTwoPlayerEnd() {
		String testString="token";
		Player player1 = new Player("name1",testString);
		Player player2 = new Player("name2",testString);
		
		player1.setMoney(200);
		player2.setMoney(100);
		
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);	
		
		game.setPlayers(players);
		game.setDebug(true);
		
		
	}
	

	
	@Test
	public void testCheck() {
		int num1 =0;
		int num2 =2147483647;//max int value
		int num3 =3;
		int num4 = -1;
		
		int[] numbers = {1,2,3,4};

		assertEquals("Wasn't in set",game.check(num1, numbers),-1);
		assertEquals("Wasn't in set",game.check(num2, numbers),-1);
		assertEquals("Was in set",game.check(num3, numbers),3);
		assertEquals("Wasn't in set",game.check(num4, numbers),-1);

	}
	
	@Test
	public void testPayRepairs() {
		Player player1 = new Player("name1","token1");
		assertEquals(game.payRepairs(100, 1000, player1),0);
				
		Property prop1 = new Property("MAVEN HEAD", 1, Colour.BROWN,60,50,2,10,30,90,160,250,30);
		Property prop2 = new Property("CORK CITY GAOIL",2, Colour.BROWN, 60,50,4,20,60,180,320,450,30);

		player1.addToHand((PurchaseableSquare)prop1);
		player1.addToHand((PurchaseableSquare)prop2);
		
		assertEquals("Repair charges aer 0",0,game.payRepairs(100, 1000, player1));
		
		prop1.buildHouse();
		
		assertEquals("Repair charges correct",100,game.payRepairs(100, 1000, player1));
		
		prop2.buildHotel();
		prop2.buildHotel();
			
		assertEquals("Repair charges are 2100",2100,game.payRepairs(100, 1000, player1));
	}
	
	@Test
	public void testSquareInteractions() throws UnsupportedEncodingException {
		
		InputStream is = System.in;
		String data =
				  "3\n"
				+ "3\n"
				+ "1\n"
				+ "CORK CITY GAOIL\n"
				+ "1\n"
				+ "CORK CITY GAOIL\n"
				+ "3\n";
		ByteArrayInputStream inp = new ByteArrayInputStream(data.getBytes());
        System.setIn(inp);
        Game game = new Game(inp);
		int incomeTax=4;
		int goToJail=30;
		int bankDeposit=38;
		
		Player player1 = new Player("name1","token1");
		
		Board board = new Board();
		
		assertFalse(game.squareInteractions(player1,board.getBoard().get(incomeTax)));		
		player1.setMoney(1);
		//requires tester to input "3" to give up
		assertTrue("Went bankrupt",game.squareInteractions(player1,board.getBoard().get(incomeTax)));
		
		player1.setMoney(2000);
		assertFalse(game.squareInteractions(player1,board.getBoard().get(bankDeposit)));		
		player1.setMoney(1);
		
		//requires tester to input "3" to give up
		assertTrue("Went bankrupt",game.squareInteractions(player1,board.getBoard().get(bankDeposit)));
		
		Property prop2 = new Property("CORK CITY GAOIL",2, Colour.BROWN, 60,50,4,20,60,180,320,450,30);
		player1.addToHand(prop2);
		player1.setMoney(199);
		
		//requires tester to input "1" to avoid bankruptcy by mortgaging square
		assertFalse("Avoided bankrupcy",game.squareInteractions(player1, board.getBoard().get(incomeTax)));

		prop2.setMortaged(false);
		player1.setMoney(1);
		
		//requires tester to input "1" to avoid bankruptcy by mortgaging square
		assertTrue("Went to bankrupcy",game.squareInteractions(player1, board.getBoard().get(incomeTax)));
		
		player1.setPosition(0);
		assertFalse(game.squareInteractions(player1,board.getBoard().get(goToJail)));
		assertTrue(player1.isInJail());
		
	    is = null;
	    System.setIn(System.in);
	}
	
	@Test
	public void testClosestTo() {
		int [] transportSquares = new int[] {5,15,25,35};		
		assertEquals("Closest square is current square",5, game.closestTo(39, transportSquares));
		assertEquals("If mid way between, goes forward",35, game.closestTo(30, transportSquares));
		assertEquals("If mid way between, goes forward",25, game.closestTo(20, transportSquares));
		
		//locations of chance card squares
		assertEquals("Closest transport",15, game.closestTo(7, transportSquares));
		assertEquals("Closest transport",25, game.closestTo(22, transportSquares));
		assertEquals("Closest transport",5, game.closestTo(36, transportSquares));

		int[] utilitySquares = new int[] {12,28};
		assertEquals("Closest utility",12, game.closestTo(7, utilitySquares));
		assertEquals("Closest utility",28, game.closestTo(22, utilitySquares));
		assertEquals("Closest utility",12, game.closestTo(36, utilitySquares));

	}
}
