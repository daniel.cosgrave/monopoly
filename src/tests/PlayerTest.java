package tests;
import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.*;
import board.*;
import player.*;

public class PlayerTest {

	@Test
	public void testGetMoney() {
		String testString="me";
		Player player = new Player(testString,testString);
	
		assertEquals("Constructs correctly",1500,player.getMoney());
	}
	
	@Test
	public void testPay() {
		String testString="me";
		Player player = new Player(testString,testString);
		Bank bank = new Bank();
		Player player2 = new Player("you","hat");

		
		player.setMoney(0);
		assertFalse("Should not be able to pay",player.pay(bank, 100));
		assertFalse("Should not be able to pay",player.pay(player2, 100));
		
		player.setMoney(200);
		
		assertTrue("Should be able to pay",player.pay(bank, 100));
		assertTrue("Should be able to pay",player.pay(player2, 100));
		assertFalse("Should not be able to pay",player.pay(player2, 100));
	}
	
	@Test
	public void testHand() {
		
		PurchaseableSquare psq1 = new PurchaseableSquare("Test1", 0, 0, 0);
		PurchaseableSquare psq2 = new PurchaseableSquare("Test2", 0, 0, 0);
		Player player = new Player("Name", "Token");
		player.addToHand(psq1);
		player.addToHand(psq2);
		
		assertEquals("Square 1 not in hand", psq1, player.getHand().get(0));
		assertTrue("Hand Size Not Changed", 2==player.getHand().size());
		player.removeFromHand(psq1);
		assertFalse("Square 1 not removed from hand", psq1==player.getHand().get(0));
	}
	
	@Test
	public void testMove() {
		
		Player player = new Player("Name", "Token");
		int[] roll1 = player.twoDiceRoll();
		
		assertEquals("Player position is not 0", 0, player.getPosition());
		
		player.movePlayer(roll1);
		
		int newPosition1 = (roll1[0] + roll1[1])%40;
		
		assertEquals("Player position is not changed", newPosition1, player.getPosition());
		
		player.setPosition(39);
		player.movePlayer(roll1);
		assertTrue("Player position has not gone a full circle", player.getPosition() < 40);
	}
	
	@Test
	public void testGoTo() {
		Player player = new Player("Name", "Token");
		player.setMoney(0);

		player.goTo(5);
		assertEquals("Player position is not changed", 5, player.getPosition());
		
		player.goTo(10);
		assertEquals("Player position is not changed", 10, player.getPosition());
		assertEquals("Player money is not changed", 0, player.getMoney());
		
		player.goTo(1);
		assertEquals("Player position is not lower", 1, player.getPosition());
		assertEquals("Player money is not changed", 200, player.getMoney());
	}
	
	@Test
	public void testPayAll() {
		Player player1 = new Player("Name1", "Token1");
		Player player2 = new Player("Name2", "Token2");
		Player player3 = new Player("Name3", "Token3");
		
		player1.setMoney(20);
		player2.setMoney(0);
		player3.setMoney(0);
		
	
		
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		players.add(player3);
		
		players.get(0).payAll(players, 10);
		
		assertEquals("Player money is not changed correctly", 0, players.get(0).getMoney());
		assertEquals("Player money is not changed correctly", 10, players.get(1).getMoney());
		assertEquals("Player money is not changed correctly", 10, players.get(2).getMoney());
		
		assertFalse("Player paid but did not have the money to",player1.payAll(players, 10));
		
		assertEquals("Player money is not changed correctly", 0, players.get(0).getMoney());
		assertEquals("Player money is not changed correctly", 10, players.get(1).getMoney());
		assertEquals("Player money is not changed correctly", 10, players.get(2).getMoney());
	}
	
	@Test
	public void testSumPoints() {
		Player player = new Player("Name", "Token");
		player.setMoney(10);
		
		Utilities util = new Utilities("Name", 10, 0, 0);
		
		player.addToHand(util);
		
		
		assertEquals("Points not added up correctly",10+util.getPrice(),player.sumPoints());
		
	}
	
	@Test
	public void testGoToJail() {
		Player player = new Player("myname","mytoken");
		int jail = 10;
		player.setPosition(jail);
		
		player.GoToJail();
		
		assertEquals(jail,player.getPosition());
		assertTrue(player.isInJail());
	}
	
	@Test
	public void testGetOutOfJail() {
		int jail=10;
		Player player = new Player("myname","mytoken");
		player.GoToJail();
		
		player.getOutOfJail();
		assertEquals(jail,player.getPosition());
		assertFalse(player.isInJail());
	}
	
	@Test
	public void testTwoDiceRoll() {
		Player player = new Player("myname","mytoken");
		int[]tdr = player.twoDiceRoll();
				
		assertNotEquals("Cannot have a roll be 0",0,tdr[0]);
		assertNotEquals("Cannot have a roll be 0",0,tdr[1]);

	}
	
}
